import React from "react";
import AuthLayout from "@/layouts/auth";
import LoginForm from "@/components/Auth/LoginForm/LoginForm";

function auth() {
    return (
        <AuthLayout>
            <LoginForm />
        </AuthLayout>
    )

}

export default auth