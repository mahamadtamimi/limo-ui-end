'use client'
import styles from '@/styles/HomeComponent.module.scss'
import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination} from "swiper/modules";
import MovieCart from './MovieCart';


// @ts-ignore
export default function DynamicSliderCart(props) {
    // @ts-ignore

    const listItems = props.data.map(item =>

        <SwiperSlide key={`${item.slug}-${item.id}`}>
            <MovieCart data={item}/>
        </SwiperSlide>
    );
    return <div className={styles.slider_cart_sec}>
        <Swiper
            slidesPerView={2}
            spaceBetween={24}
            navigation={true}
            modules={[Pagination, Navigation]}
            pagination={{
                clickable: true,
            }}
            breakpoints={{
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 50,
                },
            }}
            className={`mySwiper cartSlider ${styles.cart_slider_padding}`}>

            {props.children}

            {listItems}

        </Swiper>

    </div>
}