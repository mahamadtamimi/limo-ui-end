import type {AppProps} from "next/app";

import {NextUIProvider} from "@nextui-org/react";
import {ThemeProvider as NextThemesProvider} from "next-themes";
import {fontSans, fontMono} from "@/config/fonts";
import {useRouter} from 'next/router';
import Router from 'next/router'
import "@/styles/globals.css";
import "@/styles/globals.scss";
import "@/styles/sweaper.css"
import "@/styles/cartSweaper.css"
import React, {useEffect, useState} from "react";
import Loader from "@/components/Global/Loader/Loader";
import 'react-toastify/dist/ReactToastify.css';
import localFont from 'next/font/local'
import {ToastContainer} from "react-toastify";
import {Provider} from "react-redux";
import {persistor, store} from "@/reduxStore/store";
import { PersistGate } from 'redux-persist/integration/react'
export default function App({Component, pageProps}: AppProps) {


	const router = useRouter();
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		Router.events.on("routeChangeStart", (url) => {
			setIsLoading(true)
		});

		Router.events.on("routeChangeComplete", (url) => {
			setIsLoading(false)
		});

		Router.events.on("routeChangeError", (url) => {
			setIsLoading(false)
		});

	}, [Router])

	return (
		<NextUIProvider navigate={router.push}>
			<NextThemesProvider>
				<Provider store={store}>
					<PersistGate loading={null} persistor={persistor}>
						{isLoading && <Loader/>}
						<ToastContainer

							position="top-right"
							autoClose={5000}
							hideProgressBar={false}
							newestOnTop={false}
							closeOnClick
							rtl={true}
							pauseOnFocusLoss
							draggable
							pauseOnHover
							theme="dark"

						/>
						<Component {...pageProps} />
					</PersistGate>
				</Provider>


			</NextThemesProvider>
		</NextUIProvider>
	);
}

const YekanBakh = localFont({
	src: '../layouts/YekanBakh-VF.ttf',
})
export const fonts = {
	sans: fontSans.style.fontFamily,
	mono:
	fontMono.style.fontFamily,
	yekan:
	YekanBakh.style.fontFamily,
};
