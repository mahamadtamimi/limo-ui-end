import DefaultLayout from "@/layouts/default";
import styles from "@/styles/search.module.scss"
import {Button, Card, CardBody, Input, Link, Select, SelectItem, Tab, Tabs, Slider, Spinner} from "@nextui-org/react";
import {useState} from "react";
import MovieCart from "@/components/Home/global/MovieCart";
import {ArowDownIcon} from "@/components/ArowDownIcon";
import {Snippet} from "@nextui-org/snippet";
import {ArowTopIcon} from "@/components/ArowTopIcon";

export default function search() {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [selected, setSelected] = useState("login");
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState({
        title: '',
        release_date: '',
        age_certification: '',
        imdb_sku: '',
        limo_rate: '',
        imdb_rate: ''
    })


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [imdb, setImdb] = useState("");
    const handleSelectionChange = (e: any) => {
        setImdb(e.target.value);
    };

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [data, setData] = useState({
        show: false,
        media: null
    })
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [filterShow, setFilterShow] = useState(true)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [limo, setLimo] = useState(new Set([]));
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [value, setValue] = useState([1994, 2024]);
    console.log(imdb)

    function handleSubmit(e: any) {
        e.preventDefault()

        setInLoad(true)
        setFilterShow(false)

        const form = new FormData();


        form.append('title', formData.title);
        form.append('imdb_rate', imdb);
        // form.append('imdb_sku', 'test');
        //
        // form.append('limo_rate', '8');
        // form.append('age_certification', 'R');
        //

        // @ts-ignore
        form.append('release_date_from', value[0]);
        // @ts-ignore
        form.append('release_date_to', value[1]);


        fetch(`${process.env.API_PATH}/api/v1/search`, {
            method: "POST",
            // headers :{
            //     contentType: "application/json",
            // },
            body: form
        }).then(res => res.json()).then(data => {
            setInLoad(false)
            if (data.success) {
                setData({
                    ...data, show: true, media: data.data
                })
                setFilterShow(false)
            }
        })


    }


    function handleSerialsSubmit(e :any){

        setInLoad(true)
        setFilterShow(false)

        const form = new FormData();


        form.append('title', formData.title);
        form.append('imdb_rate', imdb);
        // @ts-ignore
        form.append('serials', true);
        //
        // form.append('limo_rate', '8');
        // form.append('age_certification', 'R');
        //

        // @ts-ignore
        form.append('release_date_from', value[0]);
        // @ts-ignore
        form.append('release_date_to', value[1]);


        fetch(`${process.env.API_PATH}/api/v1/search`, {
            method: "POST",
            // headers :{
            //     contentType: "application/json",
            // },
            body: form
        }).then(res => res.json()).then(data => {
            setInLoad(false)
            if (data.success) {
                setData({
                    ...data, show: true, media: data.data
                })
                setFilterShow(false)
            }
        })

    }

    function setForm(e: any) {
        const role = e.target.getAttribute("data-role")

        switch (role) {
            case "title":
                setFormData({
                    ...formData,
                    title: e.target.value,

                })
                break
            case "imdb_rate" :
                setFormData({
                    ...formData,
                    imdb_rate: e.target.value,
                })
                break
            case "limo_rate" :
                setFormData({
                    ...formData,
                    limo_rate: e.target.value,
                })
                break
            case "imdb_sku" :
                setFormData({
                    ...formData,
                    imdb_sku: e.target.value,
                })
                break
            case "age_certification" :
                setFormData({
                    ...formData,
                    age_certification: e.target.value,
                })
                break
            case "release_date" :
                setFormData({
                    ...formData,
                    release_date: e.target.value,
                })
                break
        }

    }

    const animals = [
        {key: "4", label: "بالای 4"},
        {key: "5", label: "بالای 5"},
        {key: "6", label: "بالای 6"},
        {key: "7", label: "بالای 7"},
        {key: "8", label: "بالای 8"},
        {key: "9", label: "بالای 9"},
    ];
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [inLoad, setInLoad] = useState(false)

    return <DefaultLayout>
        <div className={styles.main_sections}>


            <div className={`flex w-full flex-col`}>


                <div className={`flex flex-col mb-6 mt-0 `}>
                    <h6 className={`${styles.main_title} mb-6`} onClick={() => setFilterShow(!filterShow)}>

                        جستجو پیشرفته

                        {filterShow ? <ArowDownIcon/> : <ArowTopIcon/>}

                    </h6>
                    {filterShow &&
                        <Card className={`max-w-full ${styles.padding_cintroller} p-20 ${styles.set_tab_background}`}>
                            <CardBody className="overflow-hidden">
                                <Tabs
                                    fullWidth
                                    size="md"
                                    aria-label="Tabs form"
                                    selectedKey={selected}
                                    // @ts-ignore
                                    onSelectionChange={setSelected}
                                    color={'secondary'}
                                    classNames={{
                                        'tabList': 'tab-list-class',
                                        'tab': 'tab-class',
                                    }}
                                >
                                    <Tab key="serials" title="جستجو در فیلم ها" className={'text-black'}>
                                        <form className="flex flex-col gap-4 mt-12 " onSubmit={(e) => handleSubmit(e)}>


                                            <div className="flex gap-5 flex-wrap">
                                                <div className={styles.div_sections_main}>

                                                    <div className={'mb-6'}>
                                                        <h6 className={'font-bold text-white'}>
                                                            عنوان
                                                        </h6>
                                                    </div>
                                                    <div>

                                                        <Input data-role={'title'}
                                                               label={'عنوان فارسی یا اصلی'}
                                                               onChange={(e) => setForm(e)}
                                                               className={`flex-1 ${styles.set_flex_full}`}
                                                               value={formData.title}/>


                                                    </div>


                                                </div>


                                                <div className={styles.div_sections_main}>
                                                    <div className={'mb-6 mt-6'}>
                                                        <h6 className={'font-bold text-white'}>
                                                            بر اساس امتیاز
                                                        </h6>
                                                    </div>
                                                    <div className={styles.div_sections}>
                                                        {/*<Select*/}
                                                        {/*    label="امتیاز لیمو"*/}
                                                        {/*    variant="solid"*/}
                                                        {/*    // placeholder="Select an animal"*/}
                                                        {/*    selectedKeys={imdb}*/}
                                                        {/*    onSelectionChange={setLimo}*/}
                                                        {/*    className={'bg-create'}*/}
                                                        {/*    color={'primary'}*/}
                                                        {/*>*/}
                                                        {/*    {animals.map((animal) => (*/}
                                                        {/*        <SelectItem key={animal.key}>*/}
                                                        {/*            {animal.label}*/}
                                                        {/*        </SelectItem>*/}
                                                        {/*    ))}*/}
                                                        {/*</Select>*/}


                                                        <Select
                                                            label="امتیاز imdb"
                                                            // @ts-ignore
                                                            variant="solid"
                                                            // placeholder="Select an animal"
                                                            selectedKeys={[imdb]}
                                                            onChange={handleSelectionChange}
                                                            className={'bg-create'}
                                                            color={'primary'}
                                                        >
                                                            {animals.map((animal) => (
                                                                <SelectItem key={animal.key}>
                                                                    {animal.label}
                                                                </SelectItem>
                                                            ))}
                                                        </Select>
                                                    </div>


                                                </div>


                                                <div className={styles.main_sec_two_col}>
                                                    <div className={`${styles.div_sections_main} ${styles.set_hulf}`}>
                                                        <div className={'mb-6 mt-6'}>
                                                            <h6 className={'font-bold text-white'}>
                                                                سال ساخت
                                                            </h6>
                                                        </div>
                                                        <div className={'w-full'}>
                                                            <Slider


                                                                step={1}
                                                                maxValue={2024}
                                                                minValue={1994}
                                                                value={value}
                                                                // @ts-ignore
                                                                onChange={setValue}
                                                                className="w-full"
                                                                color={'secondary'}
                                                            />
                                                            <p className="text-default-500 font-medium text-small mt-5">
                                                                از - تا
                                                                : {Array.isArray(value) && value.map((b) => `${b}`).join(" – ")}
                                                            </p>
                                                        </div>
                                                    </div>


                                                    <div className={`${styles.div_sections_main} ${styles.set_hulf}`}>

                                                        <div className={'mb-6 mt-6'}>
                                                            <h6 className={'font-bold text-white'}>
                                                                رده سنی
                                                            </h6>
                                                        </div>
                                                        <div className={styles.div_sections}>
                                                            <Select
                                                                label="رده سنی"
                                                                // @ts-ignore
                                                                variant="solid"
                                                                // placeholder="Select an animal"
                                                                selectedKeys={imdb}
                                                                // @ts-ignore
                                                                onSelectionChange={setImdb}
                                                                className={'bg-create'}
                                                                color={'primary'}
                                                            >
                                                                {animals.map((animal) => (
                                                                    <SelectItem key={animal.key}>
                                                                        {animal.label}
                                                                    </SelectItem>
                                                                ))}
                                                            </Select>


                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div>
                                                <Button type="submit" className={styles.search_btn}>
                                                    جستجو
                                                </Button>
                                            </div>

                                        </form>
                                    </Tab>
                                    <Tab key="sign-up" title="جستجو در سریال ها" >
                                        <form className="flex flex-col gap-4 mt-12 " onSubmit={(e) => handleSerialsSubmit(e)}>


                                            <div className="flex gap-5 flex-wrap">
                                                <div className={styles.div_sections_main}>

                                                    <div className={'mb-6'}>
                                                        <h6 className={'font-bold text-white'}>
                                                            عنوان
                                                        </h6>
                                                    </div>
                                                    <div>

                                                        <Input data-role={'title'}
                                                               label={'عنوان فارسی یا اصلی'}
                                                               onChange={(e) => setForm(e)}
                                                               className={`flex-1 ${styles.set_flex_full}`}
                                                               value={formData.title}/>


                                                    </div>


                                                </div>


                                                <div className={styles.div_sections_main}>
                                                    <div className={'mb-6 mt-6'}>
                                                        <h6 className={'font-bold text-white'}>
                                                            بر اساس امتیاز
                                                        </h6>
                                                    </div>
                                                    <div className={styles.div_sections}>
                                                        {/*<Select*/}
                                                        {/*    label="امتیاز لیمو"*/}
                                                        {/*    variant="solid"*/}
                                                        {/*    // placeholder="Select an animal"*/}
                                                        {/*    selectedKeys={imdb}*/}
                                                        {/*    onSelectionChange={setLimo}*/}
                                                        {/*    className={'bg-create'}*/}
                                                        {/*    color={'primary'}*/}
                                                        {/*>*/}
                                                        {/*    {animals.map((animal) => (*/}
                                                        {/*        <SelectItem key={animal.key}>*/}
                                                        {/*            {animal.label}*/}
                                                        {/*        </SelectItem>*/}
                                                        {/*    ))}*/}
                                                        {/*</Select>*/}


                                                        <Select
                                                            label="امتیاز imdb"
                                                            // @ts-ignore
                                                            variant="solid"
                                                            // placeholder="Select an animal"
                                                            selectedKeys={[imdb]}
                                                            onChange={handleSelectionChange}
                                                            className={'bg-create'}
                                                            color={'primary'}
                                                        >
                                                            {animals.map((animal) => (
                                                                <SelectItem key={animal.key}>
                                                                    {animal.label}
                                                                </SelectItem>
                                                            ))}
                                                        </Select>
                                                    </div>


                                                </div>


                                                <div className={styles.main_sec_two_col}>
                                                    <div className={`${styles.div_sections_main} ${styles.set_hulf}`}>
                                                        <div className={'mb-6 mt-6'}>
                                                            <h6 className={'font-bold text-white'}>
                                                                سال ساخت
                                                            </h6>
                                                        </div>
                                                        <div className={'w-full'}>
                                                            <Slider


                                                                step={1}
                                                                maxValue={2024}
                                                                minValue={1994}
                                                                value={value}
                                                                // @ts-ignore
                                                                onChange={setValue}
                                                                className="w-full"
                                                                color={'secondary'}
                                                            />
                                                            <p className="text-default-500 font-medium text-small mt-5">
                                                                از - تا
                                                                : {Array.isArray(value) && value.map((b) => `${b}`).join(" – ")}
                                                            </p>
                                                        </div>
                                                    </div>


                                                    <div className={`${styles.div_sections_main} ${styles.set_hulf}`}>

                                                        <div className={'mb-6 mt-6'}>
                                                            <h6 className={'font-bold text-white'}>
                                                                رده سنی
                                                            </h6>
                                                        </div>
                                                        <div className={styles.div_sections}>
                                                            <Select
                                                                label="رده سنی"
                                                                // @ts-ignore
                                                                variant="solid"
                                                                // placeholder="Select an animal"
                                                                selectedKeys={imdb}
                                                                // @ts-ignore
                                                                onSelectionChange={setImdb}
                                                                className={'bg-create'}
                                                                color={'primary'}
                                                            >
                                                                {animals.map((animal) => (
                                                                    <SelectItem key={animal.key}>
                                                                        {animal.label}
                                                                    </SelectItem>
                                                                ))}
                                                            </Select>


                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div>
                                                <Button type="submit" className={styles.search_btn}>
                                                    جستجو
                                                </Button>
                                            </div>

                                        </form>
                                    </Tab>
                                </Tabs>
                            </CardBody>
                        </Card>

                    }

                </div>
            </div>


            {inLoad ?
                <div className={styles.main_sections}>
                    <Spinner color={'warning'}/>
                </div>
                :
                <div className={styles.media_styles}>
                    {

                        // @ts-ignore
                        data.show && data.media.map((item) => {
                            return <MovieCart key={`movie-${item.id}`} data={item}/>
                        })}

                </div>


            }
        </div>
    </DefaultLayout>
}