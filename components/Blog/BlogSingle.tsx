import styles from '@/styles/singleBlog.module.scss'
import image from '@/public/blog/image 7 (14).png'
import Image from "next/image";
import {CalenderIcon} from "@/components/Blog/icon/CalenderIcon";
import {DocumentIcon} from "@/components/Blog/icon/DocumentIcon";
import {Edit} from "@nextui-org/shared-icons";
import {EditIcon} from "@/components/Blog/icon/EditIcon";
import {SpanLargeIcon} from "@/components/Blog/icon/SpanLargeIcon";
import {SpanSmallIcon} from "@/components/Blog/icon/SpanSmallIcon";
import image2 from "@/public/blog/image 8.png"
import {data} from "autoprefixer";

export default function BlogSingle(props: any) {
    console.log(props.data)
    return <>
        <div className={styles.said_bar}>

            <div className={styles.container}>
                <div className={styles.right_bar}>
                    <h6 className={styles.title_right_bar}>
                        {props.data.title.rendered}
                    </h6>
                    <Image src={image} alt={''}/>
                    <div className={styles.tag}>
                        <CalenderIcon/>
                        <p className="test5">۱۸ شهریور ۱۴۰۲</p>
                        <DocumentIcon/>
                        <p className="test5">فیلم</p>
                        <EditIcon/>
                        <p className="test5">عباس چهاردهی</p>
                    </div>
                    <p className={styles.text9}>
                        «مارتین اسکورسیزی» با انتقاد از فیلم‌های «دنیای سینمایی مارول» خشم
                        هواداران کتاب‌های کامیک را برانگیخت. او در مصاحبه با مجله‌ی «امپایر»
                        در پاسخ به این پرسش که آیا فیلم‌های استودیوی مارول را دیده است یا نه
                        گفت: «سعی کرده‌ام، می‌دانید؟ اما این فیلم‌ها سینما نیستند. راستش را
                        بخواهید، شبیه‌ترین چیز به این فیلم‌ها، هرچقدر هم که خوش‌ساخت باشند و
                        بازیگرانش در بالاترین سطح ظاهر شوند، شهر بازی است. این سینمایی
                        انسانی نیست که سعی داشته باشد تجارب روانی و احساسی را به انسانی دیگر
                        منتقل کند.»
                    </p>
                    <div className={styles.matlab}>
                        <div className={styles.text1}>
                            <SpanLargeIcon/>
                            <p className={styles.text10}>فهرست مطالب</p>
                        </div>
                        <div className={styles.text1}>
                            <SpanSmallIcon/>
                            <p>مل گیبسون</p>
                        </div>
                        <div className={styles.text1}>
                            <SpanSmallIcon/>
                            <p>نقدها</p>
                        </div>
                        <div className={styles.text1}>
                            <SpanSmallIcon/>
                            <p>جودی فاستر</p>
                        </div>
                    </div>
                    <p className={styles.text9}>
                        «مارتین اسکورسیزی» با انتقاد از فیلم‌های «دنیای سینمایی مارول» خشم
                        هواداران کتاب‌های کامیک را برانگیخت. او در مصاحبه با مجله‌ی «امپایر»
                        در پاسخ به این پرسش که آیا فیلم‌های استودیوی مارول را دیده است یا نه
                        گفت: «سعی کرده‌ام، می‌دانید؟ اما این فیلم‌ها سینما نیستند. راستش را
                        بخواهید، شبیه‌ترین چیز به این فیلم‌ها، هرچقدر هم که خوش‌ساخت باشند و
                        بازیگرانش در بالاترین سطح ظاهر شوند، شهر بازی است. این سینمایی
                        انسانی نیست که سعی داشته باشد تجارب روانی و احساسی را به انسانی دیگر
                        منتقل کند.»
                    </p>
                    <h6 className={styles.title_right_bar2}>مل گیبسون</h6>
                    <Image src={image2} alt={''}/>
                    <p className={styles.text9}>
                        انتقاد از فیلم‌های ابرقهرمانی در مقابل اقدامات و اظهارات جنجالی دیگر
                        گیبسون هیچ است. او در سال ۲۰۱۶ در حین برگزاری کنفرانس‌های مطبوعاتی
                        «سه‌تیغ هکسا» (Hacksaw Ridge) در جشنواره‌ی فیلم ونیز به تندی از فیلم
                        «بتمن در برابر سوپرمن: طلوع عدالت» (Batman V Superman: Dawn of
                        Justice) انتقاد کرده و گفته بود: «آشغال است. من به این چیزها
                        علاقه‌ای ندارم. می‌دانید تفاوت ابرقهرمانان واقعی با ابرقهرمانان
                        کتاب‌های مصور در چیست؟ ابرقهرمانان واقعی اسپندکس نمی‌پوشند. بنابراین
                        نمی‌دانم. اسپندکس باید بخش زیادی از هزینه‌ها را شامل شود.»
                    </p>
                    <p className={styles.text9}>
                        جالب اینکه گیبسون یکی از گزینه‌های اصلی کارگردانی «بتمن» به
                        کارگردانی «تیم برتون» در سال ۱۹۸۹ بوده و نیز نقش «ثور» و «اودین» به
                        او پیشنهاد شده است.
                    </p>
                    <h6 className="title-right-bar2">جودی فاستر</h6>
                    <img className="img1" src="assets/img/image 8 (1).png" alt=""/>
                    <p className="text9">
                        فاستر که سریال «آینه سیاه» (Black Mirror) و فیلم «سگ آبی» (The
                        Beaver) را کارگردانی کرده، می‌گوید میانه‌ای با ابرقهرمانان ندارد. او
                        در مصاحبه با ریدیو تایمز گفته است: «عرضه‌ی محتوای بی‌ارزش از طرف
                        استودیوها برای راضی نگه‌داشتن توده‌ها و سهام‌داران مانند عمل فرکینگ
                        است. سرمایه فوراً بازمی‌گردد اما زمین نابود می‌شود. من تمایلی ندارم
                        یک فیلم ۲۰۰ میلیون دلاری در مورد ابرقهرمانان بسازم.»
                    </p>
                    <div className={styles.chasb}>
                        <p>برچسب ها:</p>
                        <div className={styles.test4}>فیلم</div>
                        <div className={styles.test4}>سریال</div>
                        <div className={styles.test4}>مارول</div>
                    </div>
                </div>
                <div className="left-bar">

                    <div className="card2">
                        <div className="text2">
                            <img src="assets/img/Rectangle 1 (1).svg" alt=""/>مطالب برتر
                        </div>
                        <div className="text3">
                            <div className="right">
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className="left">
                                <p>
                                    بابی‌کور نمی‌میرد؛ فلورنس پیو و اندرو گارفیلد در فشن شوی
                                    Vale...
                                </p>
                                <p className="date">۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className="br"></div>
                        </div>
                        <div className="text4">
                            <div className="right">
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className="left">
                                <p>برندگان جشنواره سن سباستین ۲۰۲۳</p>
                                <p className="date">۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className="br"></div>
                        </div>
                        <div className="text5">
                            <div className="right">
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className="left">
                                <p>
                                    شکایت یکی از بازیگران سیاهی لشکر فیلم بایگانی شده Batgirl
                                    از...
                                </p>
                                <p className="date">۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className="br"></div>
                        </div>
                        <div className="text6">
                            <div className="right">
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className="left">
                                <p>فصل پانزدهم SpongeBob SquarePants ساخته می‌شود</p>
                                <p className="date">۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className="br"></div>
                        </div>
                        <div className="text7">
                            <div className="right">
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className="left">
                                <p>اولین تریلر فیلم The Toxic Avenger</p>
                                <p className="date">۱۴۰۲/۰۷/۲۳</p>
                            </div>
                        </div>
                    </div>

                    <div className="card4">
                        <div className="card-header4">
                            <img src="assets/img/Rectangle 6.png" alt=""/>
                        </div>
                        <div className="card-body4">
                            <h6>لذت تماشای فیلم آنلاین</h6>
                            <p>
                                با خرید اشتراک از لیمو مووی میتوانید هر زمان با هر دستگاهی بصورت
                                از تماشای فیلم لذت ببرید.
                            </p>
                            <div className="yellow-bottun2">خرید اشتراک</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.br}></div>
        </div>
        <div className={styles.main}>
            <div className="container-main">
                <img src="assets/img/Rectangle 1.svg" alt=""/>
                <h6 className="title-main1">مطالب مرتبط</h6>
                <div className="section">
                    <div className="card-section1">
                        <img src="assets/img/image 7 (12).png" alt=""/>
                        <p className="tessst">
                            فیلم‌هایی که بیشترین خطا را در هر دقیقه داشته‌اند: The ...
                        </p>
                        <p className="text8">
                            «پرندگان» هیچکاک رکورددار بیشترین خطا در هر دقیقه است. فیلم
                            «پرندگان» (The Birds) به کار...
                        </p>
                        <div className="test2">
                            <img src="assets/img/calendar.svg" alt=""/>
                            <p className="date">۱۸ شهریور ۱۴۰۲</p>
                            <p className="read">مطالعه بیشتر</p>
                            <img
                                src="assets/img/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg"
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="card-section1">
                        <img src="assets/img/image 7 (9).png" alt=""/>
                        <p className="tessst">
                            ادای احترام دنیل ردکلیف و جی.کی. رولینگ به مایکل گا...
                        </p>
                        <p className="text8">
                            نویسنده و بازیگران «هری پاتر» به «مایکل گامبون» ادای احترام کردند.
                            «دنیل ردکلیف» و دیگر عوامل فیلم‌...
                        </p>
                        <div className="test2">
                            <img src="assets/img/calendar.svg" alt=""/>
                            <p className="date">۱۸ شهریور ۱۴۰۲</p>
                            <p className="read">مطالعه بیشتر</p>
                            <img
                                src="assets/img/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg"
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="card-section1">
                        <img src="assets/img/image 7 (5).png" alt=""/>
                        <p className="tessst">اولین تریلر فیلم The Toxic Avenger</p>
                        <p className="text8">
                            تریلر «انتقام‌جوی سمی» رونمایی شد. استودیوی لجندری تریلر رد بند
                            فیلم «انتقام‌جوی سمی» (The Toxic Ave...
                        </p>
                        <div className="test2">
                            <img src="assets/img/calendar.svg" alt=""/>
                            <p className="date">۱۸ شهریور ۱۴۰۲</p>
                            <p className="read">مطالعه بیشتر</p>
                            <img
                                src="assets/img/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg"
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="card-section1">
                        <img src="assets/img/image 7 (6).png" alt=""/>
                        <p className="tessst">
                            تریلر مستند Planet Earth III با آهنگسازی هانس زیمر
                        </p>
                        <p className="text8">
                            اولین تریلر فصل سوم «سیاره زمین» منتشر شد. بی‌بی‌سی آمریکا از
                            اولین تریلر مستند «سیاره زمین ۳» (Plan...
                        </p>
                        <div className="test2">
                            <img src="assets/img/calendar.svg" alt=""/>
                            <p className="date">۱۸ شهریور ۱۴۰۲</p>
                            <p className="read">مطالعه بیشتر</p>
                            <img
                                src="assets/img/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg"
                                alt=""
                            />
                        </div>
                    </div>
                </div>
                <img src="assets/img/Rectangle 1.svg" alt=""/>
                <div className="title-main2">
                    <h6>فیلم‌های پیشنهادی</h6>
                    <div className="icons2">
                        <img src="assets/img/Frame 48097071.svg" alt=""/>
                        <img src="assets/img/Frame 48097070.svg" alt=""/>
                    </div>
                </div>
                <div className="section2">
                    <div className="card-section2">
                        <img className="head-img" src="assets/img/image 5.png" alt=""/>
                        <div className="icons">
                            <img src="assets/img/Wishlist Delete.svg" alt=""/>
                            <img src="assets/img/Wishlist Delete (1).svg" alt=""/>
                        </div>
                        <div className="imdb">IMDB:۶.۳</div>
                        <p>Invasion 2021</p>
                    </div>
                    <div className="card-section2">
                        <img className="head-img" src="assets/img/image 5 (1).png" alt=""/>
                        <div className="icons">
                            <img src="assets/img/Wishlist Delete.svg" alt=""/>
                            <img src="assets/img/Wishlist Delete (1).svg" alt=""/>
                        </div>
                        <div className="imdb">IMDB:۷.۲</div>
                        <p>Foundation 2021</p>
                    </div>
                    <div className="card-section2">
                        <img className="head-img" src="assets/img/s-l1200 1.png" alt=""/>
                        <div className="icons">
                            <img src="assets/img/Wishlist Delete.svg" alt=""/>
                            <img src="assets/img/Wishlist Delete (1).svg" alt=""/>
                        </div>
                        <div className="imdb">IMDB:۷.۲</div>
                        <p>Air 2023</p>
                    </div>
                    <div className="card-section2">
                        <img className="head-img" src="assets/img/s-l1200 1 (1).png" alt=""/>
                        <div className="icons">
                            <img src="assets/img/Wishlist Delete.svg" alt=""/>
                            <img src="assets/img/Wishlist Delete (1).svg" alt=""/>
                        </div>
                        <div className="imdb">IMDB:۷.۲</div>
                        <p>Oppenheimer 2023</p>
                    </div>
                    <div className="card-section2">
                        <img className="head-img" src="assets/img/s-l1200 1 (2).png" alt=""/>
                        <div className="icons">
                            <img src="assets/img/Wishlist Delete.svg" alt=""/>
                            <img src="assets/img/Wishlist Delete (1).svg" alt=""/>
                        </div>
                        <div className="imdb">IMDB:۷.۲</div>
                        <p>Spider-Man: Across The Spider-Verse 2023</p>
                    </div>
                    <div className="card-section2">
                        <img className="head-img" src="assets/img/s-l1200 1 (3).png" alt=""/>
                        <div className="icons">
                            <img src="assets/img/Wishlist Delete.svg" alt=""/>
                            <img src="assets/img/Wishlist Delete (1).svg" alt=""/>
                        </div>
                        <div className="imdb">IMDB:۷.۲</div>
                        <p>The Shawshank Redemption 1994</p>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.soalat}>
            <div className="container-soalat">
                <img src="assets/img/Rectangle 1.svg" alt=""/>
                <h6 className="title-soalat">سوالات متداول</h6>
                <div className="section-soalat">
                    <div className="card-section3">
                        <div className="part8">
                            <p>چگونه باید فیلم ها را دانلود کنم؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                        <div className="part8">
                            <p>چگونه باید فیلم ها را دانلود کنم؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                        <div className="part8">
                            <p>در سیستم عامل Mac Os چگونه دانلود کنم؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                        <div className="part8">
                            <p>فیلم را دانلود کرده ولی زیرنویس ندارد؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                    </div>
                    <div className="card-section3">
                        <div className="part8">
                            <p>فیلم دوبله فارسی دانلود کرده ام اما دوبله نمیباشد؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                        <div className="part8">
                            <p>فیلم را دانلود کرده ام ولی فقط صدا/تصویر پخش میشود؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                        <div className="part8">
                            <p>آیا فیلم ها و سریال ها دارای حذفیات میباشد؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                        <div className="part8">
                            <p>رمز اکانت خود را فراموش کرده ام چه کاری باید انجام دهم؟</p>
                            <img src="assets/img/arrow-down.svg" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.nazarat}>
            <div className="container-nazarat">
                <img src="assets/img/Rectangle 1.svg" alt=""/>
                <h6 className="title-nazarat">نظرات کاربران</h6>
                <div className="sabt-nazar">
                    <p>نام و نام خانوادگی</p>
                    <p>ایمیل</p>
                    <div className="sabt1">علی عزتی</div>
                    <div className="sabt1">mail@example.com</div>
                    <p>نظر</p>
                    <div className="sabt2">نظر خود را بنویسید</div>
                    <div className="send">ارسال</div>
                </div>
                <div className="nazar1">
                    <div className="test6">
                        <h6>کیوان نادری</h6>
                        <p>۱۴۰۲/۰۷/۰۷</p>
                    </div>
                    <p className="text11">
                        منم نوجوون بودم خیلی نگاه میکردم فیلم های مارولو ولی وقتی جدی سینما
                        رو دنبال میکنی دیدن این فیلم ها واست عذاب آور میشه اینا فقط واسه سود
                        ساخته میشه و واسه مردم عامه پسند و به نظر من خیلیم نمیتونه دووم
                        بیاره مارول. یجایی ترمزش کشیده میشه حتما
                    </p>
                    <div className="icons-nazar">
                        <p>۰</p>
                        <img src="assets/img/like.svg" alt=""/>
                        <p className="number">۰</p>
                        <img src="assets/img/dislike.svg" alt=""/>
                        <img className="img2" src="assets/img/undo.svg" alt=""/>
                        <p className="text12">پاسخ</p>
                    </div>
                </div>
                <div className="nazar1">
                    <div className="test6">
                        <h6>پویا</h6>
                        <p>۱۴۰۲/۰۷/۰۷</p>
                    </div>
                    <p className="text11">
                        یه نگاه به سال تولدشون بکنند بد نیست فیلم ابر قهرمانی برای نوجوان ها
                        و کسایی که روح نوجوانیشون زنده مونده است
                    </p>
                    <div className="icons-nazar">
                        <p>۰</p>
                        <img src="assets/img/like.svg" alt=""/>
                        <p className="number">۰</p>
                        <img src="assets/img/dislike.svg" alt=""/>
                        <img className="img2" src="assets/img/undo.svg" alt=""/>
                        <p className="text12">پاسخ</p>
                    </div>
                </div>
                <div className="nazar1">
                    <div className="test6">
                        <h6>سامان</h6>
                        <p>۱۴۰۲/۰۷/۰۷</p>
                    </div>
                    <p className="text11">
                        واقعا درست گفتن تقریبا نود درصد ساخته های مارول اونم در خوشبینانه
                        ترین حالت چیزی جز آشغال نییتن!
                    </p>
                    <div className="icons-nazar">
                        <p>۰</p>
                        <img src="assets/img/like.svg" alt=""/>
                        <p className="number">۰</p>
                        <img src="assets/img/dislike.svg" alt=""/>
                        <img className="img2" src="assets/img/undo.svg" alt=""/>
                        <p className="text12">پاسخ</p>
                    </div>
                </div>
                <div className="nazar1">
                    <div className="test6">
                        <h6>ارسلان احمدی</h6>
                        <p>۱۴۰۲/۰۷/۰۷</p>
                    </div>
                    <p className="text11">
                        همه ژانرهای سینمایی طرفدارای خودشو داره و کسی رو مجبور نکردن برن
                        فیلمای ابرقهرمانی رو ببینه من به شخصه تقریبا طرفدار همه جور ژانری
                        هستم و هم ددپول و اسپایدرمن میبینم هم آیریش من باید به تمام سلیقه ها
                        احترام گذاشت و مردم رو مجبور نکنن که کدوم فیلم خوبه کدوم فیلم بد
                        البته اینکه گاسپار نوئه هم با سابقه ساختن یه فیلمی مثل Love از این
                        فیلما متنفره خیلی جالبه
                    </p>
                    <div className="icons-nazar">
                        <p>۰</p>
                        <img src="assets/img/like.svg" alt=""/>
                        <p className="number">۰</p>
                        <img src="assets/img/dislike.svg" alt=""/>
                        <img className="img2" src="assets/img/undo.svg" alt=""/>
                        <p className="text12">پاسخ</p>
                    </div>
                </div>
                <div className="nazar1">
                    <div className="test6">
                        <h6>سامان</h6>
                        <p>۱۴۰۲/۰۷/۰۷</p>
                    </div>
                    <p className="text11">
                        من همیشه از دیدن فیلم های مارول در کنار بقیه ی فیلمها و ژانر ها لذت
                        میبردم و برای شخصیت هاش احترام قائل بودم چون مخاطبشون فقط نوجوان ها
                        نبودن- تا اینکه فیلم spider-man new home ساخته شد حالا اشتیاقش برا
                        انتظار و دانلود بماند، به معنای واقعی کلمه فاجعه و لوس بود و انگار
                        استارتش زده شد چون از بعدش فیلم های مارول آشغال تر و آشغال تر شدن
                        -نمیدونم موضوع پوله یا چی و چرا مثه قبل نیست؟نمیشه مرد عنکبوتی 2002
                        رو که دیدنش هنوزم جذابه با اینی که الان ساخته شده مقایسه کرد- هر دوش
                        ابرقهرمانیه ولی این کجا و آن کجا
                    </p>
                    <div className="icons-nazar">
                        <p>۰</p>
                        <img src="assets/img/like.svg" alt=""/>
                        <p className="number">۰</p>
                        <img src="assets/img/dislike.svg" alt=""/>
                        <img className="img2" src="assets/img/undo.svg" alt=""/>
                        <p>پاسخ</p>
                    </div>
                </div>
                <div className="nazar1">
                    <div className="test6">
                        <h6>پیمان</h6>
                        <p>۱۴۰۲/۰۷/۰۷</p>
                    </div>
                    <p className="text11">
                        همه ی شما عزیزان لطفا ساکت ! دنیای سینمایی مارول جذاب ترین فیلم ها
                        رو داره ، همیشه قرار نیست که با فیلم ها گریه کنیم احساس عاشقانه
                        داشته باشیم ، گاهی هم باید به فکر حس تخیل مون باشیم ، و اتفاقا برعکس
                        شما من فکر می کنم فیلم های مارول بسیار زیاد آموزنده هم هست ، مثل
                        اونجا که به ما یاد میده از بین 1 میلیون راه شاید فقط یکی بدرد بخوره
                        ( دیالوگ دکتر استرنج در فیلم انتقام جویان 3 ) یا اونجا که به ما یاد
                        میده اگه با هم متحد باشیم می تونیم هر شخصی رو از سره راه بر داریم (
                        مرگ مامور کلسون در فیلم انتقام جویان 1 که باعث هم پیمان شدن انتقام
                        جو ها علیه لوکی میشه ) یا اونجا که یاد میده قهرمان بودن به قد و هیکل
                        نیست بلکه به خود ما بستگی داره ( فیلم کاپیتان آمریکا 1 ) و خیلی نکات
                        آموزنده دیگه که شما فیلم سازان مثلا بزرگ اون رو نمی بینید
                    </p>
                    <div className="icons-nazar">
                        <p>۰</p>
                        <img src="assets/img/like.svg" alt=""/>
                        <p className="number">۰</p>
                        <img src="assets/img/dislike.svg" alt=""/>
                        <img className="img2" src="assets/img/undo.svg" alt=""/>
                        <p className="text12">پاسخ</p>
                    </div>
                </div>
                <div className="see-more">مشاهده بیشتر<img src="assets/img/linear/vuesax/linear/arrow-down.svg" alt=""/>
                </div>
            </div>
        </div>
    </>


}