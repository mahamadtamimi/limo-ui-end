import Link from "next/link";
import React from "react";


export default function Breadcrumb(props : any) {

    return <div className='breadcrumb'>
        <ul>
            <li>
                <Link href='/'>
                    لیمو مووی
                </Link>
            </li>
            {
                props.data.map((item : any) => (
                    <li key={`breadcrumb-${item[1]}`}>
                        <Link href={item[1]}>
                            {item[0]}
                        </Link>
                    </li>
                ))
            }
        </ul>
    </div>
}