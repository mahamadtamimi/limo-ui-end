import styles from '@/styles/HomeComponent.module.scss'
import Image from "next/image";

import {useEffect, useState} from "react";
import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import StaticSliderHead from "@/components/Home/Genres/StaticSliderHead";
import CountryCart from "@/components/Home/Country/CountryCart";


export default function Country() {

    const [data, setData] = useState()
    const [pending, setPending] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/tags`).then(res => res.json()).then(data => {
            setData(data)
            setPending(false)
        });
    }, []);





    return <>
        {!pending &&
            // @ts-ignore
            data.length > 0 &&
            <div className={styles.country_sec}>

                <StaticSliderHead title='برترین دسته بندی ها' subtitle='تماشای فیلم براساس دسته‌بندی دلخواه شما'/>
                <div className={styles.country_grid}>
                    { // @ts-ignore
                        data.map(item =>
                        <CountryCart key={item.id} data={item}/>
                    )}
                </div>
            </div>


        }

    </>


}