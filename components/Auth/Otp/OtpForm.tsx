import {Link} from "@nextui-org/link";
import styles from "@/styles/auth.module.scss";
import Image from "next/image";
import arrowRight from "@/public/arrow-right.svg";
import {toast, ToastContainer} from "react-toastify";
import {Button, Input} from "@nextui-org/react";
import React, {useEffect, useLayoutEffect, useRef, useState} from "react";
import {useRouter} from "next/router";

import {initialData} from "./initialData";
import 'react-toastify/dist/ReactToastify.css';
import success = toast.success;
import {useDispatch, useSelector} from "react-redux";


export default function OtpForm() {

    const dispatch = useDispatch();


    const userPersist = useSelector((state: any) => state.user)


    const initialsData: any = initialData
    const router = useRouter()
    const [inLoad, setInLoad] = useState(false)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [formData, setFormData] = useState(initialsData)


    const [timer, setTimer] = useState(300)
    const [start, setStart] = useState(true)

    const [getNewCode, setGetNewCode] = useState(false)

    // console.log(userPersist)
    async function handleSubmit(event: any) {
        event.preventDefault()
        setInLoad(true)
        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})
        const formDataCollection = new FormData(event.currentTarget)
        const code = formDataCollection.get('code')
        console.log({phoneNumber: userPersist.phone, code: code})

        try {
            const data = await fetch(`${process.env.API_PATH}/api/v1/auth/validate-code`, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({phoneNumber: userPersist.phone, code: code}),
            })
            if (data.status == 200) {

                const result = await data.json()


                console.log(result)

                if (result.success) {
                    toast.dismiss('registerToastId')
                    toast.success('ورود موفقیت امیز')


                    dispatch({
                        type: 'SET_TOKEN',
                        payload: {
                            token: result.token,

                        }
                    })

                    router.push('/auth/dashboard/main')
                } else {
                    toast.dismiss('registerToastId')
                    toast.error('کد نامعتبر !')

                }
            }


        } catch (e) {
            toast.dismiss('registerToastId')
            toast.error('خطای سرور')
        }


        setInLoad(false)
        // console.log(data)
        setInLoad(false)
    }

    function sendCode() {
        setStart(false)
        setInLoad(true)

        toast.loading('لطفا منتظر باشید !', {toastId: "registerToastId"})


        fetch(`${process.env.API_PATH}/api/v1/auth/get-code`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({phoneNumber: userPersist.phone}),
        }).then(res => res.json()).then(data => {
            console.log(data)
            setTimer(data.time_validate ? data.time_validate : 300)
            toast.dismiss('registerToastId')
            toast.success('کد جدید با موفقیت ارسال شد .')
            setInLoad(false)
            setStart(false)
            console.log(data)
            return data;
            // if (data.status == 200) {
            //
            // }
        })


    }
    const initialized = useRef(false)


    useEffect(() => {
        // console.log('ok');
        console.log(start)
        if (!initialized.current) {
            initialized.current = true
            sendCode()
            setTimer(300)
        }
    }, []);


    useEffect(() => {
        if (timer === 0) {
            setGetNewCode(true)
            setTimer(300)

        }

        if (!getNewCode) {
            const interval = setInterval(() => {
                setTimer(timer - 1)
            }, 1000);
            return () => clearInterval(interval);

        }


    }, [timer, getNewCode]);


    function checkFormData(e: any) {
        // console.log(e.target)


        const code = e.target.value
        const valid = code.length == 6;
        if (code.length > 6) return
        const updateDate: any = {
            ...formData,
            code: {
                ...formData.code,
                value: e.target.value,
                validate: valid
            }
        }

        setFormData(updateDate)


    }


    return <>
        <Link className={styles.back_link} onClick={() => router.back()}>
            <Image src={arrowRight} alt={''} width={20}/>
            تغییر شماره
        </Link>


        <p className={styles.login_section_prag}>رمز یک بار مصرف</p>
        <div className={styles.login_main_sec}>
            <form onSubmit={handleSubmit}>

                <div className={`${styles.inputSection}`}>
                    <Input
                        name={'code'}
                        value={formData.code.value}
                        variant="bordered"
                        isInvalid={!formData.code.validate}
                        color={!formData.code.validate ? "danger" : "success"}
                        errorMessage={!formData.code.validate && formData.code.error}

                        onChange={(e) => checkFormData(e)}
                        type="text"
                        data-name={'code'}
                        label="کد شش رقمی"

                        labelPlacement='inside'

                    />
                </div>


                <Button

                    isLoading={inLoad}
                    disabled={!(formData.code.validate && formData.code.validate)}
                    type="submit"

                    className={`${styles.login_btn_sign_up} ${!(formData.code.validate && formData.code.validate) && styles.login_btn_login_disable}`}>
                    ورود
                </Button>

                <Button
                    onClick={sendCode}
                    isLoading={inLoad}
                    disabled={!getNewCode}
                    className={`${styles.login_btn_sign_up} ${!getNewCode && styles.login_btn_login_disable}`}>


                    {
                        getNewCode ?
                            <span>
                                      ارسال مجدد کد
                            </span>
                            :
                            <span>

                                ارسال مجدد کد :

                                {timer}
                                ثانیه
                        </span>

                    }

                </Button>
            </form>
        </div>

    </>
}