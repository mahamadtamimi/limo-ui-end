import styles from '@/styles/blog.module.scss'
import DefaultLayout from "@/layouts/default";
import imageBanner from '@/public/blog/Limo Movie Website UI (Copy) (2) 2/vuesax/linear/vuesax/linear/Vector.svg'
import Image from "next/image";
import img1 from '@/public/blog/Rectangle 1 (4).svg'
import img2 from '@/public/blog/image 7 (13).png'
import img3 from '@/public/blog/Limo Movie Website UI (Copy) (3) 2/vuesax/linear/arrow-left.svg'
import img4 from '@/public/blog/Rectangle 1 (1).svg'
import img5 from '@/public/blog/Rectangle 1 (2).svg'
import img6 from '@/public/blog/image 7 (1).png'
import img7 from '@/public/blog/calendar.svg'
import img8 from '@/public/blog/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg'
import {BlogArchive} from "@/components/Blog/BlogArchive";

export default function Index() {

    return <DefaultLayout>

        <BlogArchive/>
    </DefaultLayout>


}