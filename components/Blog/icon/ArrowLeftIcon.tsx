

export const ArrowLeftIcon = () => (
    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M6.37992 3.95331L2.33325 7.99998L6.37992 12.0466" stroke="#FFD100" strokeWidth="1.5"
              strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M13.6668 8H2.44678" stroke="#FFD100" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round"
              strokeLinejoin="round"/>
    </svg>

)