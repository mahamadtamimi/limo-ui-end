import styles from '../../../styles/mainMenu.module.scss';
import Link from "next/link";
import Image from "next/image";
import userAvatar from "../../../public/userAvatar.svg";
import arowDown from "../../../public/arowDown.svg";
import noti from "../../../public/noti.svg";
import getMe from "@/api/getMe";
import {Button, ButtonGroup} from "@nextui-org/react";
// import {cookies} from 'next/headers'

// import getTrailers from "../../api/getTrailers";
// import getMe from "../../api/getMe";

export default function UserAvatar() {


    // const me = await getMe()

    return <div className={styles.top_bar_btn_section}>
                <span className={styles.notific_sec}>
                             <Image src={noti} alt='' width={40} height={40}/>
                        </span>

        <Button className={`${styles.top_bar_kharid_eshtarak}  btn_2x btn_primary`}>
            خرید اشتراک
        </Button>

        <Button className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
            <Link href='/auth/login'>
                لاگین
            </Link>

        </Button>


    </div>
}