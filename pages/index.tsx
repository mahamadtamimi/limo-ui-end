import {Link} from "@nextui-org/link";
import {Snippet} from "@nextui-org/snippet";
import {Code} from "@nextui-org/code";
import {button as buttonStyles} from "@nextui-org/theme";
import {siteConfig} from "@/config/site";
import {title, subtitle} from "@/components/primitives";
import {GithubIcon} from "@/components/icons";
import DefaultLayout from "@/layouts/default";
import MainSlider from "@/components/Home/MainSliderServerComponent/MainSlider";
import NewSerials from "@/components/Home/NewSerials/NewSerials";
import NewMovies from "@/components/Home/NewMovies/NewMovies";
import TrailerSlider from "@/components/Home/TrailerSlider/TrailerSlider";
import SortInIMDB from "@/components/Home/SortInIMDB/SortInIMDB";
import Banner from "@/components/Home/Banner/Banner";
import Genres from "@/components/Home/Genres/Genres";
import MostVisited from "@/components/Home/MostVisited/MostVisited";
import Country from "@/components/Home/Country/Country";
import React, {useEffect, useState} from "react";
import {data} from "autoprefixer";
import {Spinner} from "@nextui-org/react";

export default function IndexPage() {
    const [data, setData] = useState()

    const [pending, setPending] = useState(true)
    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/index-info`)
            .then(res => res.json())
            .then(data => {


                if (data.success) {
                    setPending(false)
                    setData(data.data)
                }


            })
    }, []);


    console.log('test')


    return (
        <DefaultLayout>
            {!pending ?
                <>
                    {// @ts-ignore
                        data.sliders.length && <MainSlider data={data.sliders}/>}
                    {// @ts-ignore
                        data.serials.length && <NewSerials data={data.serials}/>}
                    {// @ts-ignore
                        data.movies.length && <NewMovies data={data.movies}/>}
                </>
                :
                <Spinner color="warning"/>

            }


            {/*<TrailerSlider/>*/}
            {/*<SortInIMDB/>*/}
            {/*<Banner/>*/}
            {/*<Genres/>*/}
            {/*<MostVisited/>*/}

            {/*<Country/>*/}
        </DefaultLayout>
    );
}
