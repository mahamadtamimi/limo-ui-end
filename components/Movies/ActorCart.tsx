import styles from "@/styles/movie.module.scss";
import Image from "next/image";

// @ts-ignore
export default function ActorCart(props) {


    return <div className={styles.slider_cart}>
        <div className={styles.slider_cart_image}>
            {props.data.image_src &&
                <Image className={styles.image_src} src={`http://127.0.0.1:8000/storage/${props.data.image_src}`} alt=''
                       width={180}
                       height={258}/>

            }

        </div>

        <h2 className={styles.slider_cart_name}>
            {props.data.full_name}
        </h2>
    </div>
}