import styles from "@/styles/movie.module.scss";
import Image from "next/image";
import dlBox from "@/public/Movir-vuesax-linear-import.svg";
import ardw from "@/public/Movir-vuesax-linear-arrow-up.svg";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import {Button} from "@nextui-org/react";
import {useDispatch, useSelector} from "react-redux";

export default function DownLoadBox(props: any) {

    const user = useSelector((state: any) => state.user)
    const token = useSelector((state: any) => state.token)
    const [inLoad, setInLoad] = useState(true)
    const dispatch = useDispatch();
    const [userActivePlan, setUserActivePlan] = useState()


    const [btnMood, setBtnMood] = useState(0)




    useEffect(() => {
        // @ts-ignore
        if (!user || !token) {
            setInLoad(false)
            return
        }


        fetch(`${process.env.API_PATH}/api/v1/auth/me`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then(res => {
            if (res.status !== 200) {
                // dispatch({
                //     type: 'LOGOUT'
                // })
                // return null
            }
            return res.json()
        }).then((data) => {

            if (data.success) {
                // setBtnMood(1)

            } else {
                // dispatch({
                //     type: 'LOGOUT'
                // })
            }


        })

        fetch(`${process.env.API_PATH}/api/v1/plans/my-plans`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then(res => {

                return res.json()
            }
        ).then((data) => {

            setInLoad(false)
            if (data.success) {
               setBtnMood(2)
            } else {
                setBtnMood(1)
            }
            // data if (data.error) setError(true)
            // setError setResponse(data)
            //  setPending(false)

        })


    }, []);


    const Btn = (props :any) => {


        if (!inLoad) {
            // @ts-ignore

            if (btnMood === 0) {
                return <Button href="/auth/login"
                               as={Link}
                               className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                    لاگین
                </Button>
            }
            // @ts-ignore
            if (btnMood === 1) {
                return <Button href="/auth/dashboad/main"
                               as={Link}
                               className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                    خرید پلن
                </Button>
            }
            // @ts-ignore
            if (btnMood === 2) {
                return <Button href={props.link}
                               as={Link}
                               className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                    دانلود
                </Button>
            }
            return null

        }

    }




    return <div className={styles.download_box}>
        <div className={styles.download_box_head}>
            <Image src={dlBox} alt='' width={40} height={40}/>
            <h3 className={styles.download_box_head_title}>
                لینک‌های دانلود
            </h3>

        </div>


        {

            Object.keys(props.source).map(function (key) {
                // console.log(props.source[key])
                return <div key={`item-${key}`} className={styles.download_box_part}>

                    <div className={styles.download_box_part_head}>
                        <h4 className={styles.download_box_part_head_title}>
                            {key === '' && 'دانلود'}
                            {key === 'main_lang' && 'زبان اصلی'}
                            {key === 'dubbed' && 'دوبله'}
                            {key === 'subtitle' && 'زیر نویس'}

                        </h4>

                        <Image src={ardw} alt='' width={24} height={24}/>

                    </div>
                    {
                        props.source[key].map((item: any) => (

                            <div key={`subtitle-${item.id}`} className={styles.download_box_part_body}>
                                <div className={styles.download_box_part_main_btn_sec}>
                                    < // @ts-ignore
                                        Btn link={item.src}/>


                                </div>
                                <div className={styles.download_box_part_info}>
                                <span>
                                         زبان اصلی
                                </span>
                                    <p className={styles.download_box_part_info_large}>{item.quality}</p>
                                    <div>
                                        <p className={styles.download_box_part_info_small}>
                                            size : {item.size} MB
                                        </p>
                                    </div>
                                </div>

                            </div>
                        ))
                    }


                </div>


            })
        }




    </div>

}