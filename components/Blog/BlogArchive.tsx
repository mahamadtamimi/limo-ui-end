import styles from "@/styles/blog.module.scss";
import Image from "next/image";
import imageBanner from "@/public/blog/Limo Movie Website UI (Copy) (2) 2/vuesax/linear/vuesax/linear/Vector.svg";
import img1 from "@/public/blog/Rectangle 1 (4).svg";
import img2 from "@/public/blog/image 7 (13).png";
import img3 from "@/public/blog/Limo Movie Website UI (Copy) (3) 2/vuesax/linear/arrow-left.svg";
import img4 from "@/public/blog/Rectangle 1 (1).svg";
import img5 from "@/public/blog/Rectangle 1 (2).svg";
import img6 from "@/public/blog/image 7 (1).png";
import img7 from "@/public/blog/calendar.svg";
import img8 from "@/public/blog/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg";
import {useEffect, useState} from "react";
import {CalenderIcon} from "@/components/Blog/icon/CalenderIcon";

import * as shamsi from 'shamsi-date-converter';
import {ArrowLeftIcon} from "@/components/Blog/icon/ArrowLeftIcon";
import Link from "next/link";

export function BlogArchive() {
    const [inLoad, setInLoad] = useState(true)
    const [data, setData] = useState({})


    useEffect(() => {
        fetch(`${process.env.BLOG_URL}/wp/v2/posts`, {}).then(res => res.json()).then(data => {
            setData(data)
            setInLoad(false)
            console.log(data)
        })


    }, [])


    return <>
        <div className={styles.header}>
            <div className={styles.container}>
                <div className={styles.test1}>
                    <p>لیمو مووی</p>
                    <Image src={imageBanner} alt={''}/>
                    <p className={styles.text1}

                    >وبلاگ</p>
                </div>
                <Image src={img1} alt={''}/>
                <h6 className={styles.title_header}
                >
                    منتخب ماه
                </h6>

                <div className={styles.head}>
                    <div className={styles.card_header}>
                        <Image src={img2} alt={''}/>
                    </div>
                    <div className={styles.card_body}>
                        <h6 className={styles.card_body_title}>
                            بهترین سریال‌هایی که سپتامبر ۲۰۲۳ پخش می‌شوند
                        </h6>
                        <p>
                            در آخرین روزهای فصل تابستان و در حالی که اعتصاب نویسندگان و
                            بازیگران هالیوود همچنان ادامه دارد و استودیوها حاضر به بازگشت به
                            میز مذاکره نیستند، پخش سریال‌های جدید ادامه پیدا می‌کند. در ماه
                            سپتامبر امسال سریال جدید «نسل وی» که اسپین‌آفی برای «پسران» آمازون
                            است پخش خواهد شد اما از سوی دیگر سریالی چون...
                        </p>
                        <div className={styles.bottun}>
                            <h6>مطالعه بیشتر</h6>
                            <Image src={img3} alt={''}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.said_bar}>
            <div className={styles.container}>
                <div className={styles.right_bar}>
                    <div className={styles.card1}>
                        <ul>
                            <li>
                                <Image src={img4} alt={''}/>
                                <p>دسته‌بندی‌ها</p>
                            </li>
                            <li>
                                <Image src={img5} alt={''}/>
                                <p>فیلم</p>
                            </li>
                            <li>
                                <Image src={img5} alt={''}/>
                                <p>سریال</p>
                            </li>
                            <li>
                                <Image src={img5} alt={''}/>
                                <p>اخبار</p>
                            </li>
                            <li>
                                <Image src={img5} alt={''}/>
                                <p>باکس آفیس</p>
                            </li>
                        </ul>
                    </div>
                    <div className={styles.card2}>
                        <div className={styles.text2}>
                            <img src="assets/img/Rectangle 1 (1).svg" alt=""/>

                            مطالب برتر
                        </div>
                        <div className={styles.text3}>
                            <div className={styles.right}>
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className={styles.left}>
                                <p>
                                    بابی‌کور نمی‌میرد؛ فلورنس پیو و اندرو گارفیلد در فشن شوی
                                    Vale...
                                </p>
                                <p className={styles.date}>۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className={styles.br}></div>
                        </div>
                        <div className={styles.text4}>
                            <div className={styles.right}>
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className={styles.left}>
                                <p>برندگان جشنواره سن سباستین ۲۰۲۳</p>
                                <p className={styles.date}>۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className={styles.br}></div>
                        </div>
                        <div className={styles.text5}>
                            <div className={styles.right}>
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className={styles.left}>
                                <p>
                                    شکایت یکی از بازیگران سیاهی لشکر فیلم بایگانی شده Batgirl
                                    از...
                                </p>
                                <p className={styles.date}>۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className={styles.br}></div>
                        </div>
                        <div className={styles.text6}>
                            <div className={styles.right}>
                                <img src="assets/img/Rectangle 1 (2).svg" alt=""/>
                            </div>
                            <div className={styles.left}>
                                <p>فصل پانزدهم SpongeBob SquarePants ساخته می‌شود</p>
                                <p className={styles.date}>۱۴۰۲/۰۷/۲۳</p>
                            </div>
                            <div className={styles.br}></div>
                        </div>

                    </div>
                    <div className={styles.card3}>
                        <div className="text2">
                            <img src="assets/img/Rectangle 1 (1).svg" alt=""/>عضویت در
                            خبرنامه
                        </div>
                        <p>
                            با عضویت در خبرنامه ما از آخرین اخبار سینمای ایران و جهان و همچنین
                            از تخفیفات باخبر شوید.
                        </p>
                        <p>لطفا ایمیل خود را وارد کنید.</p>
                        <div className="email">mail@example.com</div>
                        <div className="sabt">ثبت ایمیل</div>
                    </div>
                    <div className={styles.card4}>
                        <div className="card-header4">
                            <img src="assets/img/Rectangle 6.png" alt=""/>
                        </div>
                        <div className="card-body4">
                            <h6>لذت تماشای فیلم آنلاین</h6>
                            <p>
                                با خرید اشتراک از لیمو مووی میتوانید هر زمان با هر دستگاهی بصورت
                                از تماشای فیلم لذت ببرید.
                            </p>
                            <div className="yellow-bottun2">خرید اشتراک</div>
                        </div>
                    </div>
                </div>
                <div className={styles.left_bar}>
                    <img className={styles.tesst} src="assets/img/Rectangle 1 (4).svg" alt=""/>
                    <h6 className={styles.title_left_bar}>
                        مطالب اخیر</h6>
                    <div className={styles.section}>
                        {    // @ts-ignore
                            !inLoad && data.map((item, index) => {
                                return <div key={item.id} className={styles.card_section1}>
                                    <Link href={`/blog/${item.id}`} className={styles.cart_link}>
                                        <Image src={img6} alt=""/>
                                        <p className={styles.tessst}>
                                            {item.title.rendered}
                                        </p>
                                        <div className={styles.text8}
                                             dangerouslySetInnerHTML={{__html: item.excerpt.rendered}}/>

                                        <div className={styles.test2}>

                                            <p className={styles.cart_date}>
                                                <CalenderIcon/>
                                                <span>
                                            {shamsi.gregorianToJalali(item.date).join('/')}
                                        </span>

                                            </p>
                                            <p className={styles.read}>
                                        <span>
                                              مطالعه بیشتر
                                        </span>


                                                <ArrowLeftIcon/>
                                            </p>

                                        </div>


                                    </Link>

                                </div>


                            })}


                    </div>
                    <div className="number">
                        <div className="number1">۱</div>
                        <p>۲</p>
                        <p>۳</p>
                        <p>۴</p>
                        <img src="assets/img/Frame 48097201.svg" alt=""/>
                        <p>۵</p>
                        <div className="test3">
                            <p className="next">بعدی</p>
                            <img
                                src="assets/img/Limo Movie Website UI (Copy) (4) 2/vuesax/linear/vuesax/linear/arrow-left.svg"
                                alt=""
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
}