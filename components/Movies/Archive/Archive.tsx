import Image from "next/image";
import cover from "@/public/image 1.png"
import {inspect} from "util";
import styles from './page.module.scss'

import MovieCart from "@/components/Home/global/MovieCart";
import {useEffect, useState} from "react";
import Breadcrumb from "@/components/Global/Breadcrumb";
import {Default} from "ag-charts-community/dist/types/src/util/default";
import DefaultLayout from "@/layouts/default";

export default function Archive() {
    const [data, setData] = useState()
    const [inLoad, setInLoad] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/movie/list`, {}).then(res => res.json()).then(data => {
            if (data.success) {
                setInLoad(false);
                setData(data.data)
            }

        })
    }, []);



    const breadCrumb = [
        ['فیلم ها', 'movie'],
    ]


    return <DefaultLayout>
        <div className={styles.archive_main_sec}>

            <Image className={styles.archive_cover} src={cover} alt='' width={2000} height={700}/>
            <Breadcrumb data={breadCrumb}/>

            <h1 className={styles.main_title}>
                فیلم‌ها
            </h1>
            <div className={styles.filter}>
                <select name="" id="">
                    <option value="">test</option>
                    <option value="">test</option>
                    <option value="">test</option>
                </select>

                <select name="" id="">
                    <option value="">test</option>
                    <option value="">test</option>
                    <option value="">test</option>
                </select>
            </div>
            <div className={styles.archive_main_sec_grid}>
                {

                    !inLoad &&
                    // @ts-ignore
                    data.map((item) => {
                        return <MovieCart key={`movie-${item.id}`} data={item}/>
                    })

                }
            </div>
        </div>
    </DefaultLayout>


}