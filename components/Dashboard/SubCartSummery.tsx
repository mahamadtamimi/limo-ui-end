import styles from "@/styles/dashboard.module.scss";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {Button, Spinner} from "@nextui-org/react";
import {Link} from "@nextui-org/link";
import {useSelector} from "react-redux";
import {AgChartsReact} from 'ag-charts-react';
import {buildStyles, CircularProgressbar} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

export default function SubCartSummery(props: {}) {

    const router = useRouter()


    const user = useSelector((state: any) => state.user)
    const token = useSelector((state: any) => state.token)
    const [activePlan, setActivePlan] = useState(false)
    const [pending, setPending] = useState(true)


    const [error, setError] = useState(false)

    const [response, setResponse] = useState()

    // const user = useSelctor
    useEffect(() => {

        fetch(`${process.env.API_PATH}/api/v1/plans/my-plans`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }

        }).then(res => res.json()).then((data) => {
            // if (data.error) setError(true)
            // setResponse(data)
            // setPending(false)
            if (data.success) {
                setActivePlan(data.data)

            }
            setPending(false)
            console.log(data)


        })


    }, []);

    let date1 = new Date();
    // @ts-ignore
    let date2 = new Date(activePlan.expire);


    let Difference_In_Time =
        date2.getTime() - date1.getTime();


    let Difference_In_Days =
        Math.round
        (Difference_In_Time / (1000 * 3600 * 24));

    function Example(props: any) {
        return (
            props.children
        );
    }

    return <>
        {pending ? <Spinner color={'warning'}/> :
            // @ts-ignore
            !activePlan ?
                <>
                    <div className={styles.main_dashboard_sub_sec}>

                        <div className={styles.main_dashboard_sub_sec__empty}>
                            <p className={`${styles.main_dashboard_sub_prag} text-center`}>اشتراک شما غیرفعال است.</p>
                            <Button as={Link}
                                    href="/auth/dashboard/buy-sub"
                                    className={styles.login_btn_login}>
                                خرید اشتراک
                            </Button>
                        </div>
                    </div>
                </>
                :
                <>
                    <div className={styles.main_dashboard_sub_sec}>
                        <div className={''}>
                            <Example>
                                <CircularProgressbar
                                    value={80}
                                    strokeWidth={50}
                                    styles={buildStyles({
                                        strokeLinecap: "butt",
                                        pathColor: `#FFD100`,
                                        textColor: '#3c372f',
                                        trailColor: '#3c372f',
                                        backgroundColor: '#3c372f',
                                    })}
                                />
                            </Example>
                        </div>
                        <div>
                            <p className={styles.main_dashboard_sub_prag}>
                                اشتراک

                                طلایی برای شما فعال است .
                                {/*<span className={styles.sub_name}>{response.data.name}</span>*/}

                            </p>
                            <span className={styles.main_dashboard_sub_sub_prag}>
                                <span>
                                    تاریخ انقضا:
                                </span>
                                <span>
                                     {Difference_In_Days}
                                </span>
                                <span>
                                    روز دیگر
                                </span>

                    </span>
                        </div>
                    </div>
                </>
        }


    </>
}