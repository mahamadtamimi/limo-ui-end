import styles from '../../../styles/mainMenu.module.scss';
import Image from "next/image";

import arowDown from '../../../public/arowDown.svg'

import LIMO from '../../../public/LIMO.svg'
import searchNormal from '../../../public/vuesax-linear-search-normal.svg'
import F48097115 from '../../../public/Frame 48097115.svg'

import {Suspense, useEffect, useState} from "react";
import UserAvatar from "@/components/Global/MainMenu/UserAvatar";
import {Link, Button, Spinner} from "@nextui-org/react";
import {redirect} from "next/navigation";
import {useRouter} from "next/router";
import noti from '@/public/noti.svg'
import icon1 from '@/public/vuesax/bulk/crown.svg'
import icon2 from '@/public/vuesax/bulk/menu.svg'
import icon3 from '@/public/vuesax/bulk/profile-circle.svg'
import icon4 from '@/public/vuesax/bulk/messages-3.svg'
import icon5 from '@/public/vuesax/bulk/logout.svg'
import moment from "moment";
import {useDispatch, useSelector} from "react-redux";


export default function MainBar() {


    const router = useRouter()
    const dispatch = useDispatch();

    const [showMenu, setShowMenu] = useState(false)
    const user = useSelector((state: any) => state.user)
    const token = useSelector((state: any) => state.token)






    // const [response, setResponse] = useState()
    const [pending, setPending] = useState(false)

    const [activePlan, setActivePlan] = useState(false)

    // const user = useSelctor


    useEffect(() => {

        // @ts-ignore


        if (!user || !token) {

            return
        }


        fetch(`${process.env.API_PATH}/api/v1/auth/me`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then(res => {
            if (res.status !== 200) {
                // dispatch({
                //     type: 'LOGOUT'
                // })
                // return null
            }
            return res.json()
        }).then((data) => {

            if (data.success) {


            }else {
                dispatch({
                    type: 'LOGOUT'
                })
            }

        })
        //
        //
        // @ts-ignore
        // setToken(token)
        // setUser(user)
        fetch(`${process.env.API_PATH}/api/v1/plans/my-plans`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }

        }).then(res => {

                return res.json()
            }
        ).then((data) => {


            if (data.success) {
                setActivePlan(data.data)
            }else {


            }
           // data if (data.error) setError(true)
           // setError setResponse(data)
           //  setPending(false)



        })


    }, []);


    function logout() {
        dispatch({
            type: 'SET_TOKEN',
            payload: {
                token: null,

            }
        })
    }

    const UserMenu = () => {
        let date1 = new Date();
        // @ts-ignore
        let date2 = new Date(activePlan.expire);


        let Difference_In_Time =
            date2.getTime() - date1.getTime();


        let Difference_In_Days =
            Math.round
            (Difference_In_Time / (1000 * 3600 * 24));


        return <div className={styles.user_menu_main_sec}>
            <ul>
                {activePlan &&
                    <li className={styles.user_menu_item}>
                        <Image src={icon1} alt={''}/>
                        {Difference_In_Days} روز اشتراک دارید.
                    </li>
                }
                <li className={''}>
                    <Link href='/auth/dashboard/main' className={styles.user_menu_item}>
                            <Image src={icon3} alt={''}/>
                        مشاهده پروفایل
                    </Link>

                </li>
                {/*<li className={styles.user_menu_item}>*/}
                {/*    <Image src={icon2} alt={''}/>*/}
                {/*    لیست تماشا*/}
                {/*</li>*/}
                {/*<li className={styles.user_menu_item}>*/}
                {/*    <Image src={icon3} alt={''}/>*/}
                {/*    ویرایش اطلاعات*/}
                {/*</li>*/}
                {/*<li className={styles.user_menu_item}>*/}
                {/*    <Image src={icon4} alt={''}/>*/}
                {/*    تیکت‌ها*/}
                {/*</li>*/}
                <li onClick={() => logout()} className={styles.user_menu_item}>
                    <Image src={icon5} alt={''}/>
                    خروج از حساب کاربری
                </li>
            </ul>
        </div>
    }


    return <>
        <div className={styles.top_bar}>
            <div className={styles.top_bar_menu}>
                <ul>
                    <li>
                    <Link href='/blog'>
                            وبلاگ
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            تماس با ما
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            درباره ما
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            قوانین و مقررات
                        </Link>
                    </li>
                    <li>
                        <Link href='/public'>
                            سوالات متداول
                        </Link>
                    </li>
                </ul>
            </div>


            <div className={styles.top_bar_btn_section}>

                {
                    pending ?
                        <Spinner color={'warning'}/>
                        :
                        <>
                            {
                                // @ts-ignore
                                !activePlan &&
                                <Button as={Link} href={'/auth/dashboard/buy-sub'}
                                        className={`${styles.top_bar_kharid_eshtarak}  btn_2x btn_primary`}>
                                    خرید اشتراک
                                </Button>
                            }

                            {
                                // @ts-ignore
                                // !response.success ?
                                <>
                                    {token ?
                                        <>

                                            <div className={styles.top_bar_user_bar_sec}
                                                 onClick={() => setShowMenu(!showMenu)}>
                                                   <span className={styles.top_bar_user_bar_avatar}>
                                                       {/*<Image src={userAvatar} alt='' width={30} height={33}/>*/}
                                                   </span>
                                                    <span className={styles.top_bar_user_bar_user}>
                                                            کیوان نادری
                                                        </span>
                                                    <span className={styles.top_bar_user_bar_user_show_menu}>
                                                            <Image src={arowDown} alt='' width={16} height={16}/>
                                                        </span>


                                                        {showMenu && <UserMenu/>}

                                                    </div>


                                            <span className={styles.notific_sec}>
                                                             <Image src={noti} alt='' width={40} height={40}/>
                                                        </span>

                                        </> :


                                        <Button href="/auth/login"
                                                as={Link}
                                                className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                                            لاگین

                                        </Button>


                                    }


                                </>
                            }
                        </>


                }


            </div>


        </div>

        <div className={styles.top_main_menu}>
            <div>
                <Link href='/'>
                    <Image src={LIMO} alt='' width={107} height={30}/>
                </Link>
            </div>

            <ul className={styles.main_menu}>
                <li>
                    <Link href='/'>
                        صفحه اصلی
                    </Link>

                </li>
                <li>
                    <Link href='/movie'>
                        فیلم ها
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
                <li>
                    <Link href='/serial'>
                        سریال ها
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
                <li>
                    <Link href=''>
                        صفحه اصلی
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
                <li>
                    <Link href=''>
                        صفحه اصلی
                        <Image src={arowDown} alt='' width={16} height={16}/>
                    </Link>

                </li>
            </ul>

            <span className={styles.search_sec}>
                <Image src={searchNormal} alt='' width={16} height={16}/>
                <input type="text"/>


                <Image src={F48097115} alt='' width={32} height={32}/>
            </span>
        </div>
    </>
}