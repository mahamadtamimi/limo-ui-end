import {nextui} from '@nextui-org/react'

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./pages/**/*.{js,ts,jsx,tsx,mdx}', './components/**/*.{js,ts,jsx,tsx,mdx}', './app/**/*.{js,ts,jsx,tsx,mdx}', './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}'],
    theme: {
        extend: {},
    },
    darkMode: "class",
    plugins: [nextui({
        themes: {
            light: {
                colors: {
                    secondary: '#FFD100',
                    default: {


                        DEFAULT: "#ffffff",
                        foreground: "#ffffff",


                    }
                },

            },

            dark: {
                colors: {
                    secondary: '#FFD100',
                    default: {

                        // 50: "#da5252",
                        100: "rgba(39,52,70,0.46)",
                        200: "#273446",
                        300: "rgba(39,52,70,0.46)",
                        // 400: "#da5252",
                        500: "#ffffff",
                        // 600: "#F182F6",
                        // 700: "#FCADF9",
                        // 800: "#FDD5F9",
                        // 900: "#FEECFE",
                        DEFAULT: "#ffffff",
                        foreground: "#515151",


                    }
                },

            }
        }
    })],
}
