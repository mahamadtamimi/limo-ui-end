import DefaultLayout from "@/layouts/default";
import {BlogArchive} from "@/components/Blog/BlogArchive";
import BlogSingle from "@/components/Blog/BlogSingle";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {data} from "autoprefixer";

export default function page() {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [inLoad, setInLoad] = useState(true)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [data, setData] = useState()
    console.log(router.query.slug)

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        if (router.query.slug) {
            fetch(`${process.env.BLOG_URL}/wp/v2/posts/${router.query.slug}`, {}).then(res => res.json()).then(data => {
                setData(data)
                setInLoad(false)
                console.log(data)
            })

        }
    }, [router])

    return <DefaultLayout>

        {!inLoad && <BlogSingle data={data}/>}
    </DefaultLayout>


}