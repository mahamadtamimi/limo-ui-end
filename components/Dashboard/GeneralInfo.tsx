import styles from '@/styles/GeneralInfo.module.scss'
import {Button, Input} from "@nextui-org/react";
import DashboardHead from "@/components/Dashboard/DashboardHead";
import {useEffect, useState} from "react";

export default function GeneralInfo() {

    const [token, setToken] = useState()

    // useEffect(() => {
    //     const tokenStorage = localStorage.getItem("token");
    //     setToken(tokenStorage)
    // })


    function SubmitHandle(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault()
        fetch(`${process.env.API_PATH}/api/general-info`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
            body: JSON.stringify({
                test: 'hello word'
            })
        }).then(res => res.json()).then(data => {console.log(data)})


        console.log('hello word')
    }


    return <section className={styles.dashboard_main_sec}>
        <DashboardHead title={'ویرایش اطلاعات'}/>

        <div className={styles.dashboard_content_sec}>
            {/*<form action="" onSubmit={(e) => SubmitHandle(e)}>*/}
                <div className={styles.info_form}>

                    <Input label={'نام'}/>
                    <Input label={'شماره موبایل'}/>
                    <Input label={'رمز عبور'}/>
                </div>

                <div className={'flex justify-end'}>
                    <Button type={'submit'} className={styles.dashboard_form_btn}>ذخیره</Button>
                </div>
            {/*</form>*/}

        </div>
    </section>
}