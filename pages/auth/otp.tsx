import React, {useState} from "react";
import AuthLayout from "@/layouts/auth";
import OtpForm from "@/components/Auth/Otp/OtpForm";

function auth() {


    return (
        <AuthLayout>
            <OtpForm/>
        </AuthLayout>
    )

}

export default auth