import {
    Button,
    Kbd,
    Link,
    Input,
    Navbar as NextUINavbar,
    NavbarContent,
    NavbarMenu,
    NavbarMenuToggle,
    NavbarBrand,
    NavbarItem,
    NavbarMenuItem, Spinner,
} from "@nextui-org/react";

import {link as linkStyles} from "@nextui-org/theme";

import {siteConfig} from "@/config/site";
import NextLink from "next/link";
import clsx from "clsx";

import {ThemeSwitch} from "@/components/theme-switch";
import {
    TwitterIcon,
    GithubIcon,
    DiscordIcon,
    HeartFilledIcon,
    SearchIcon,
} from "@/components/icons";

import {Logo} from "@/components/icons";
import styles from "@/styles/mainMenu.module.scss";
import Image from "next/image";
import arowDown from "@/public/arowDown.svg";
import noti from "@/public/noti.svg";
import {useState} from "react";
import {useRouter} from "next/router";
import {useDispatch, useSelector} from "react-redux";
import icon1 from "@/public/vuesax/bulk/crown.svg";
import icon3 from "@/public/vuesax/bulk/profile-circle.svg";
import icon5 from "@/public/vuesax/bulk/logout.svg";
// import styles from '../../../styles/mainMenu.module.scss';


export const Navbar = () => {
    const [pending, setPending] = useState(false)

    const [activePlan, setActivePlan] = useState(false)


    const router = useRouter()
    const dispatch = useDispatch();

    const [showMenu, setShowMenu] = useState(false)
    const user = useSelector((state: any) => state.user)
    const token = useSelector((state: any) => state.token)


    const searchInput = (
        <Input
            aria-label="Search"
            classNames={{
                inputWrapper: "bg-default-100",
                input: "text-sm",
            }}
            endContent={
                <Kbd className="hidden lg:inline-block" keys={["command"]}>
                    K
                </Kbd>
            }
            labelPlacement="outside"
            placeholder="Search..."
            startContent={
                <SearchIcon className="text-base text-default-400 pointer-events-none flex-shrink-0"/>
            }
            type="search"
        />
    );

    const UserMenu = () => {
        let date1 = new Date();
        // @ts-ignore
        let date2 = new Date(activePlan.expire);


        let Difference_In_Time =
            date2.getTime() - date1.getTime();


        let Difference_In_Days =
            Math.round
            (Difference_In_Time / (1000 * 3600 * 24));


        return <div className={styles.user_menu_main_sec}>
            <ul>
                {activePlan &&
                    <li className={styles.user_menu_item}>
                        <Image src={icon1} alt={''}/>
                        {Difference_In_Days} روز اشتراک دارید.
                    </li>
                }
                <li className={''}>
                    <Link href='/auth/dashboard/main' className={styles.user_menu_item}>
                        <Image src={icon3} alt={''}/>
                        مشاهده پروفایل
                    </Link>

                </li>
                {/*<li className={styles.user_menu_item}>*/}
                {/*    <Image src={icon2} alt={''}/>*/}
                {/*    لیست تماشا*/}
                {/*</li>*/}
                {/*<li className={styles.user_menu_item}>*/}
                {/*    <Image src={icon3} alt={''}/>*/}
                {/*    ویرایش اطلاعات*/}
                {/*</li>*/}
                {/*<li className={styles.user_menu_item}>*/}
                {/*    <Image src={icon4} alt={''}/>*/}
                {/*    تیکت‌ها*/}
                {/*</li>*/}
                <li onClick={() => logout()} className={styles.user_menu_item}>
                    <Image src={icon5} alt={''}/>
                    خروج از حساب کاربری
                </li>
            </ul>
        </div>
    }
    function logout() {
        dispatch({
            type: 'SET_TOKEN',
            payload: {
                token: null,

            }
        })
    }

    return (
        <NextUINavbar maxWidth="full" position="sticky" className={styles.main_menu_one}>

            <NavbarContent className="basis-1/5 sm:basis-full" justify="start">

                <div className="hidden lg:flex gap-4 justify-start ml-2">
                    <NavbarItem key={'/blog'}>
                        <NextLink
                            className={clsx(
                                linkStyles({color: "foreground"}),
                                "data-[active=true]:text-primary data-[active=true]:font-medium font-light color-a3"
                            )}
                            color="foreground"
                            href={'/blog'}
                        >
                            وبلاگ
                        </NextLink>
                    </NavbarItem>

                </div>
            </NavbarContent>

            <NavbarContent className="hidden sm:flex basis-1/5 sm:basis-full" justify="end">

                <div className={styles.top_bar_btn_section}>

                    {
                        pending ?
                            <Spinner color={'warning'}/>
                            :
                            <>
                                {
                                    // @ts-ignore
                                    !activePlan &&
                                    <Button as={Link} href={'/auth/dashboard/buy-sub'}
                                            className={`${styles.top_bar_kharid_eshtarak}  btn_2x btn_primary`}>
                                        خرید اشتراک
                                    </Button>
                                }

                                {
                                    // @ts-ignore
                                    // !response.success ?
                                    <>
                                        {token ?
                                            <>

                                                <div className={styles.top_bar_user_bar_sec}
                                                     onClick={() => setShowMenu(!showMenu)}>
                                                   <span className={styles.top_bar_user_bar_avatar}>
                                                       {/*<Image src={userAvatar} alt='' width={30} height={33}/>*/}
                                                   </span>
                                                    <span className={styles.top_bar_user_bar_user}>
                                                            کیوان نادری
                                                        </span>
                                                    <span className={styles.top_bar_user_bar_user_show_menu}>
                                                            <Image src={arowDown} alt='' width={16} height={16}/>
                                                        </span>


                                                    {showMenu && <UserMenu/>}

                                                </div>


                                                <span className={styles.notific_sec}>
                                                             <Image src={noti} alt='' width={40} height={40}/>
                                                        </span>

                                            </> :


                                            <Button href="/auth/login"
                                                    as={Link}
                                                    className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                                                لاگین

                                            </Button>


                                        }


                                    </>
                                }
                            </>


                    }


                </div>

            </NavbarContent>

            <NavbarContent className="sm:hidden basis-1 pl-4" justify="end">
                <div className={styles.top_bar_btn_section}>

                    {
                        pending ?
                            <Spinner color={'warning'}/>
                            :
                            <>
                                {
                                    // @ts-ignore
                                    !activePlan &&
                                    <Button as={Link} href={'/auth/dashboard/buy-sub'}
                                            className={`${styles.top_bar_kharid_eshtarak}  btn_2x btn_primary`}>
                                        خرید اشتراک
                                    </Button>
                                }

                                {
                                    // @ts-ignore
                                    // !response.success ?
                                    <>
                                        {token ?
                                            <>

                                                <div className={styles.top_bar_user_bar_sec}
                                                     onClick={() => setShowMenu(!showMenu)}>
                                                   <span className={styles.top_bar_user_bar_avatar}>
                                                       {/*<Image src={userAvatar} alt='' width={30} height={33}/>*/}
                                                   </span>
                                                    <span className={styles.top_bar_user_bar_user}>
                                                            کیوان نادری
                                                        </span>
                                                    <span className={styles.top_bar_user_bar_user_show_menu}>
                                                            <Image src={arowDown} alt='' width={16} height={16}/>
                                                        </span>


                                                    {showMenu && <UserMenu/>}

                                                </div>


                                                <span className={styles.notific_sec}>
                                                             <Image src={noti} alt='' width={40} height={40}/>
                                                        </span>

                                            </> :


                                            <Button href="/auth/login"
                                                    as={Link}
                                                    className={`${styles.top_bar_login}  btn_2x btn_primary_outline`}>
                                                لاگین

                                            </Button>


                                        }


                                    </>
                                }
                            </>


                    }


                </div>
                <NavbarMenuToggle/>
            </NavbarContent>

            <NavbarMenu className={'background-set'}>

                <div className="mx-4 mt-20 flex flex-col gap-2 ">
                    {siteConfig.navMenuItems.map((item, index) => (
                        <NavbarMenuItem key={`${item}-${index}`}>
                            <Link
                                color={'foreground'}
                                href="#"
                                size="lg"
                            >
                                وبلاگ
                            </Link>
                        </NavbarMenuItem>
                    ))}
                </div>
            </NavbarMenu>
        </NextUINavbar>
    );
};
