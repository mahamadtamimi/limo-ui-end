import styles from "@/styles/subsCart.module.scss";
import Image from "next/image";
import tick from "../../public/tick-square.svg";
import React, {useEffect, useState} from "react";
import Link from "next/link";
import {Button} from "@nextui-org/react";
import {toast} from "react-toastify";
import {redirect} from "next/navigation";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";
// import PaymentBtn from "../PaymentBtn/PaymentBtn";

export default function SubCart(props: any) {
    const router = useRouter()
    const [isLoad, setIsLoad] = useState(false)

    const [response, setResponse] = useState({
        status: null,
        flow: null
    })


    const user = useSelector((state: any) => state.user)
    const token = useSelector((state: any) => state.token)

    useEffect(() => {
        if (response.status === 200) {
            // @ts-ignore
            router.push(response.flow)
        }
    }, [response]);

    console.log(token)

    function buyPlan(e: any) {
        setIsLoad(true)

        fetch(`${process.env.API_PATH}/api/v1/payment/create-payment`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({plan_id: e.target.getAttribute('data-role'), token: token})
        }).then(res => {
                if (res.status !== 200) {
                    toast.error('خطایی رخ داده است!')
                    setIsLoad(false)

                }
                return res.json()
            }
        ).then(data => {
                console.log(data)

                setResponse({
                    ...response,
                    // @ts-ignore
                    status: 200,
                    flow: data.data.StartPay
                })
            }
        )

    }


    const number = props.data.price
    const formattedNumber = number.toLocaleString("en-US")
    return <div className={styles.sub_cart}>
        <p className={styles.sub_top}>{props.data.expire_day} روزه</p>
        <h3 className={styles.sub_cart_title}>{formattedNumber} تومان</h3>
        <ul className={styles.sub_cart_list}>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p> پخش آنلاین</p>
            </li>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p> دسترسی به کلیه سریال‌ها</p>

            </li>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p>بدون هرگونه سانسور و حذفیات</p>

            </li>
            <li>
                <Image src={tick} alt='' width={24} height={24}/>
                <p>پشتیبانی 24 ساعته</p>
            </li>
        </ul>
        <Button isLoading={isLoad} data-role={props.data.id} onClick={(e) => buyPlan(e)} className={styles.btn_buy}>
            خرید اشتراک
        </Button>


    </div>
}