import styles from '../../styles/dashboard.module.scss'
import avatar from '../../public/Component 10.svg'
import category from '../../public/category.svg'
import Image from "next/image";
import {Link} from "@nextui-org/link";

import logoutImg from '../../public/logout.svg'
import profileImage from '../../public/profile-circle.svg'
import massageIcon from '../../public/messages-3.svg'
export default function DashboardSideBar(){
    return <div className={styles.dashboard_sidebar}>
        <div className={styles.dashboard_sidebar_avatar}>
         <span className={styles.dashboard_sidebar_avatar_sec}>
              <Image src={avatar} alt=''/>
         </span>
            <p> کیوان نادری</p>
        </div>

        <ul className={styles.dashboard_sidebar_menu}>
            <li className={styles.m_active}>
                <Link href=''>
                    <Image src={category} alt=''/>
                    داشبورد
                </Link>
            </li>
            {/*<li>*/}
            {/*    <Link href='/dashboard/info'>*/}
            {/*        <Image src={profileImage} alt=''/>*/}
            {/*        ویرایش اطلاعات*/}
            {/*    </Link>*/}
            {/*</li>*/}
            {/*<li>*/}
            {/*    <Link href='/dashboard/ticket'>*/}
            {/*        <Image src={massageIcon} alt=''/>*/}
            {/*        تیکت‌ها*/}
            {/*    </Link>*/}
            {/*</li>*/}
            {/*<li>*/}
            {/*    <Link href=''>*/}
            {/*        <Image src={category} alt=''/>*/}
            {/*        داشبورد*/}
            {/*    </Link>*/}
            {/*</li>*/}
        </ul>
        <Link className={styles.dashboard_sidebar_menu_logout} href='/'>
            <Image src={logoutImg} alt=''/>
            خروج از حساب کاربری
        </Link>
    </div>
}