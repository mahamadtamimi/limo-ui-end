export const DocumentIcon = () => (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M17.5 5.83332V14.1667C17.5 16.6667 16.25 18.3333 13.3333 18.3333H6.66667C3.75 18.3333 2.5 16.6667 2.5 14.1667V5.83332C2.5 3.33332 3.75 1.66666 6.66667 1.66666H13.3333C16.25 1.66666 17.5 3.33332 17.5 5.83332Z"
            stroke="#A0A8B3" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M12.0835 3.75V5.41667C12.0835 6.33333 12.8335 7.08333 13.7502 7.08333H15.4168" stroke="#A0A8B3"
              strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M6.6665 10.8333H9.99984" stroke="#A0A8B3" strokeWidth="1.5" strokeMiterlimit="10"
              strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M6.6665 14.1667H13.3332" stroke="#A0A8B3" strokeWidth="1.5" strokeMiterlimit="10"
              strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

)