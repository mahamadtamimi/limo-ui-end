import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination} from "swiper/modules";
import TrailerCartSingle from "@/components/Home/TrailerSlider/TrailerCartSingle";
import styles from '@/styles/HomeComponent.module.scss'

export default function TrailerCart(props : any) {

    // @ts-ignore
    const listItems = props.data.map(item =>
        <SwiperSlide key={`trailer-${item.id}`}>
            <TrailerCartSingle data={item}/>
        </SwiperSlide>
    )
    return <div className={styles.trailer_slider_sec}>

        <Swiper
            slidesPerView={3}
            spaceBetween={24}
            navigation={true}
            modules={[Pagination, Navigation]}
            pagination={{
                clickable: true,
            }}
            className="mySwiper cartSlider">
            {props.children}

            {listItems}


        </Swiper>
    </div>
}