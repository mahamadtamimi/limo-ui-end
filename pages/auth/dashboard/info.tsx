import React from "react";
import DashboardLayout from "@/layouts/dashboard";
import GeneralInfo from "@/components/Dashboard/GeneralInfo";


export default function page({params}: { params: { slug: string } }) {

    return <DashboardLayout>
        <GeneralInfo/>
    </DashboardLayout>

}