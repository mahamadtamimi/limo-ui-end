import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import DynamicSliderHead from "@/components/Home/global/DynamicSliderHead";
import {useEffect, useState} from "react";


export default function NewMovies(props : any) {


    const [data, setData] = useState(props.data)


    // @ts-ignore
    return <DynamicSliderCart data={data}>
        <DynamicSliderHead title='جدیدترین فیلم ها' subtitle='جدیدترین فیلم ها'/>
    </DynamicSliderCart>


}