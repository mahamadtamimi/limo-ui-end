import {Head} from "./head";
import localFont from "next/font/local";
import styles from "@/styles/auth.module.scss";
import Image from 'next/image'
import LimoIcon from '../public/LIMO.svg'
import React from "react";

import MainBar from "@/components/Global/MainMenu/MainMenu";
import DashboardSideBar from "@/components/Dashboard/DashboardSideBar";

const YekanBakh = localFont({
    src: './YekanBakh-VF.ttf',
    variable: '--yekan',
})
export default function DashboardLayout({
                                            children,
                                        }: {
    children: React.ReactNode;
}) {


    return (
        <>

            <main className={`dashboard ${YekanBakh.variable}`}>
                <MainBar/>
                <div className={'dashboard_content'}>
                    <DashboardSideBar/>
                    {children}
                </div>

            </main>
            {/*<Footer/>*/}

        </>


    );
}
