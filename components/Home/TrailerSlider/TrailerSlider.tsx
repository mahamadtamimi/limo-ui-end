import {useEffect, useState} from "react";
import DynamicSliderCart from "@/components/Home/global/DynamicSliderCart";
import DynamicSliderHead from "@/components/Home/global/DynamicSliderHead";
import TrailerCart from "@/components/Home/TrailerSlider/TrailerCart";

export default function TrailerSlider() {
    const [data, setData] = useState()
    const [pending, setPending] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/latest-serials`).then(res => res.json()).then(data => {
            setData(data)
            setPending(false)
        });
    }, []);

    // @ts-ignore

    return <>
        {!pending &&
            // @ts-ignore
            data.length > 0 && <TrailerCart data={data}>
                <DynamicSliderHead title='جدیدترین ترلر ها' subtitle='جدیدترین ترلر ها'/>
            </TrailerCart>

        }

    </>
}