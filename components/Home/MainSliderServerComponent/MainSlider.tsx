'use client'
import React, {useEffect, useRef, useState} from "react";
import {Swiper, SwiperSlide} from "swiper/react";
import 'swiper/css';


import {Navigation, Pagination} from 'swiper/modules';
import MainSliderSlide from "./MainSliderSlide";
import {Spinner} from "@nextui-org/react";


// @ts-ignore
export default function MainSlider(props:any) {
    const [data, setData] = useState(props.data)

    return <Swiper
        navigation={true} modules={[Navigation, Pagination]}
        pagination={{
            clickable: true,
        }}
        className="mySwiper mainSlider">
        {// @ts-ignore
            data.map((item) => (
                <SwiperSlide key={`mainSlider-${item.media.id}`}>
                    <MainSliderSlide data={item.media}/>
                </SwiperSlide>

            ))
        }
    </Swiper>


}