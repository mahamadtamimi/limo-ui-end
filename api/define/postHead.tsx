// "use server"
// import {cookies} from "next/headers";


export default async function postHeader(data: any) {
    // async function addTodoItem() {


        // const token = cookies().get('auth')?.value

        // const headers = token ? {
        //         "Content-Type": "application/json",
        //         "token": token
        //     } :
        //     {
        //         "Content-Type": "application/json",
        //     }
        //
        //
        //   return headers;
    // }

    return {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers:   {
            "Content-Type": "application/json",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data),
    }


}