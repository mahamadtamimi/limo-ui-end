import {
    Button,
    Kbd,
    Link,
    Input,
    Navbar as NextUINavbar,
    NavbarContent,
    NavbarMenu,
    NavbarMenuToggle,
    NavbarBrand,
    NavbarItem,
    NavbarMenuItem,
} from "@nextui-org/react";

import {link as linkStyles} from "@nextui-org/theme";

import {siteConfig} from "@/config/site";
import NextLink from "next/link";
import clsx from "clsx";

import {ThemeSwitch} from "@/components/theme-switch";
import {
    TwitterIcon,
    GithubIcon,
    DiscordIcon,
    HeartFilledIcon,
    SearchIcon,
} from "@/components/icons";

import {Logo} from "@/components/icons";
import Image from "next/image";
import LIMO from "@/public/LIMO.svg";
import styles from "@/styles/mainMenu.module.scss";
import searchNormal from "@/public/vuesax-linear-search-normal.svg";
import F48097115 from "@/public/Frame 48097115.svg";

export const NavbarTwo = () => {
    const searchInput = (
        <Link href={'/search'} >
        <Input
            aria-label="Search"
            classNames={{
                inputWrapper: "bg-default-100",
                input: "text-sm",
            }}
            endContent={
                <Kbd className="hidden lg:inline-block" keys={["command"]}>
                    K
                </Kbd>
            }
            labelPlacement="outside"
            placeholder="Search..."
            startContent={
                <SearchIcon className="text-base text-default-400 pointer-events-none flex-shrink-0"/>
            }
            type="search"
        />
        </Link>
    );

    return (
        <NextUINavbar maxWidth="full" position="sticky" className={styles.main_menu_one}>
            <NavbarContent className="basis-1/5 sm:basis-full" justify="start">
                <NavbarBrand className="gap-3 max-w-fit">
                    <NextLink className="flex justify-start items-center gap-1" href="/">
                        <Image src={LIMO} alt='' width={107} height={30}/>

                    </NextLink>
                </NavbarBrand>

            </NavbarContent>

            <NavbarContent className="basis-1/5 sm:basis-full" justify="start">
                <div className="hidden lg:flex gap-8 justify-start ml-2" >

                    <NavbarItem key={'index'}>
                        <NextLink
                            className={clsx(
                                linkStyles({color: "foreground"}),
                                "data-[active=true]:text-primary data-[active=true]:font-medium"
                            )}
                            color="foreground"
                            href={'/'}
                        >
                            صفحه اصلی
                        </NextLink>
                    </NavbarItem>

                    <NavbarItem key={'serials'}>
                        <NextLink
                            className={clsx(
                                linkStyles({color: "foreground"}),
                                "data-[active=true]:text-primary data-[active=true]:font-medium"
                            )}
                            color="foreground"
                            href={'/serial'}
                        >
                            سریال ها
                        </NextLink>
                    </NavbarItem>

                    <NavbarItem key={'movie'}>
                        <NextLink
                            className={clsx(
                                linkStyles({color: "foreground"}),
                                "data-[active=true]:text-primary data-[active=true]:font-medium"
                            )}
                            color="foreground"
                            href={'/movie'}
                        >
                            فیلم ها
                        </NextLink>
                    </NavbarItem>

                </div>


            </NavbarContent>
            <NavbarContent className="hidden sm:flex basis-1/5 sm:basis-full" justify="end">
                   <Link href={'/search'} className={styles.search_sec}>
                        <Image src={searchNormal} alt='' width={16} height={16}/>
                        <input type="text" placeholder={'جستجو'}/>


                        <Image src={F48097115} alt='' width={32} height={32}/>
                    </Link>
            </NavbarContent>

            <NavbarContent className="sm:hidden basis-1 pl-4" justify="end">
                <NavbarMenuToggle/>
            </NavbarContent>

            <NavbarMenu className={'background-set'}>

                <div className="mx-4 flex flex-col gap-2 mt-20">

                    <NavbarMenuItem key={`index`}>
                        <Link
                            color={'foreground'}
                            href="#"
                            size="lg"
                        >
                            صفحه نخست
                        </Link>
                    </NavbarMenuItem>

                </div>
            </NavbarMenu>
        </NextUINavbar>
    );
};
