import Image from "next/image";
import cover from "../../public/image 1.png"

import styles from './page.module.scss'

import Link from "next/link";
import MovieCart from "@/components/Home/global/MovieCart";
import {useEffect, useState} from "react";
import Breadcrumb from "@/components/Global/Breadcrumb";
import DefaultLayout from "@/layouts/default";


// @ts-ignore
export default function SerialArchive(props) {
    const page = (props.setPage) ? props.setPage : 1
    const [data, setData] = useState()
    const [inLoad, setInLoad] = useState(true)

    useEffect(() => {
        fetch(`${process.env.API_PATH}/api/v1/serial/list`, {}).then(res => res.json()).then(data => {
            if (data.success) {
                setInLoad(false);
                setData(data.data)
            }

        })
    }, []);



    const breadCrumb = [
        ['سریال ها', 'serial'],
    ]





    // const links = data.media.links.map(item =>
    //     <li key={item.label}>
    //         <Link className={page == item.label ? styles.active_link : ''} href={`/serial/page/${item.label}`}>
    //             {parseInt(item.label) ? item.label : ''}
    //         </Link>
    //     </li>
    // );


    // const genre = data.genres.map(item =>
    //     <option key={`genre-key-${item.id}`} value={item.slug}>{item.farsi_name ? item.farsi_name : item.name}</option>
    // )
    return <DefaultLayout>
        <div className={styles.archive_main_sec}>

            <Image className={styles.archive_cover} src={cover} alt='' width={2000} height={1000}/>
            <Breadcrumb data={breadCrumb}/>

            <h1 className={styles.main_title}>
                سریال ها
            </h1>
            <div className={styles.filter}>
                <select name="" id="">
                    <option value="">مرتب سازی بر اساس</option>

                </select>

                {/*<select name="" id="">*/}
                {/*    <option value="">ژانر</option>*/}
                {/*    {genre}*/}
                {/*</select>*/}


                <button>
                    فیلتر
                </button>

            </div>
            <div className={styles.archive_main_sec_grid}>
                {

                    !inLoad &&
                    // @ts-ignore
                    data.map((item) => {
                        return <MovieCart key={`movie-${item.id}`} data={item}/>
                    })

                }
            </div>
            <div className={styles.archive_pg}>
                {/*<ul>*/}
                {/*    {links}*/}
                {/*</ul>*/}
            </div>
        </div>
    </DefaultLayout>

}