export default async function getSlider() {
    const res = await fetch(`${process.env.API_PATH}/api/v1/sliders`)

    if (!res.ok) {

        throw new Error('Failed to fetch data')
    }

    return res.json()
}