import styles from "@/styles/subsCart.module.scss";
import SubCart from "@/components/Dashboard/SubCart";
import DashboardLayout from "@/layouts/dashboard";
import {Spinner} from "@nextui-org/react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {toast} from "react-toastify";
import success = toast.success;
import {useSelector} from "react-redux";

export default function verify() {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()

    const query = router.query

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const user = useSelector((state: any) => state.user)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const token = useSelector((state: any) => state.token)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [response, setResponse] = useState({
        success: -1
    })

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [counter, setCounter] = useState(3)


    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        // @ts-ignore
        const tokenStorage: string = localStorage.getItem('token')
        if (query.Authority)
            fetch(`${process.env.API_PATH}/api/v1/payment/verify-payment`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify({Authority: query['Authority'], Status: query['Status']})
            })
                .then(res => {
                    if (res.status === 404) {
                        setResponse({
                            // @ts-ignore
                            success: false
                        })
                    }

                    return res.json();

                })
                .then(data => {
                    console.log(data)
                    if (data.success) {
                        setResponse({
                            success: 1
                        })
                    } else {
                        setResponse({
                            // @ts-ignore
                            success: false
                        })
                    }
                })
    }, [query]);


    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {

        if (response.success) {


            counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);


            if (counter == 0) {
                router.push("/auth/dashboard/main")
            }
        }
    }, [counter, response])


    return <DashboardLayout>
        <main className={styles.main_sub_page}>

            <h3 className={styles.main_sub_title}>خرید اشتراک</h3>

            <p className={styles.main_sub_sub_title}>دسترسی به هزاران فیلم و سریال با خرید اشتراک لیمو مووی</p>

            <div className={styles.alarmbox}>
                {
                    response.success == -1 && <Spinner color="warning" size={'lg'}/>
                }
                {
                    response.success == 0 && <p className={styles.unsucvess}> پرداخت ناموفق</p>
                }
                {
                    response.success === 1 && <>
                        <p className={styles.success}>پرداخت شما موفق بود . اشتراک شما فعال شد</p>
                        <span>
                            ریدایرک

                            <span>{counter}</span>

                            ثانیه
                        </span>
                    </>
                }
            </div>
        </main>


    </DashboardLayout>
}