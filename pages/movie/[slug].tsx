// import MediaInnerMainHead from "../../../../components/MediaInnerMainHead/MediaInnerMainHead";
// import getMedia from "../../../../api/getMedia";
// import styles from "@/app/page.module.css";

import {useEffect, useState} from "react";
import {useRouter} from "next/router";

import DefaultLayout from "@/layouts/default";
import MediaInnerMainHead from "@/components/Movies/MediaInnerMainHead";

export default function page({params}: { params: { slug: string } }) {
    // const data = await getMedia(params.slug)
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [data, setData] = useState()
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [pending, setPending] = useState(true)

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        if (router.query.slug) {
            fetch(`${process.env.API_PATH}/api/v1/media/${router.query.slug}`)
                .then(res => res.json())
                .then(data => {

                    setData(data.data)
                    setPending(false)


                });
        }

    }, [router]);

    return<DefaultLayout>
        <main className={''}>
            {!pending && <MediaInnerMainHead data={data}/>}
        </main>
    </DefaultLayout>
}