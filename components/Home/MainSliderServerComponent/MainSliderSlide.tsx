import Image from "next/image";
import React from "react";
import Link from "next/link";

// @ts-ignore
export default function MainSliderSlide(props) {
    const slug = props.data.serial ? `/serial/${props.data.slug}` : `/movie/${props.data.slug}`

    return <Link className='main-slide-link' href={slug}>
        <Image src={`${process.env.API_PATH}/storage/${props.data.second_cover}`} alt='' width={1000} height={300}/>
        <div className='slide-info'>
                    <span className='slide-imdb'>
                        imdb : {props.data.imdb_rate}
                    </span>
            <h2 className='main-title'>
                {props.data.name}
            </h2>
        </div>
    </Link>
}