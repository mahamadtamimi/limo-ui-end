export const ArowTopIcon = () => (
    <svg fill="#ffffff" height="25" width="25" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
         xmlnsXlink="http://www.w3.org/1999/xlink"
         viewBox="0 0 490 490" xmlSpace="preserve">
        <path d="M245,258.23l153.302,155.246L490,324.619L245,76.524L0,324.619l91.697,88.857L245,258.23z M43.502,324.14L245,120.101
	l201.498,204.04l-47.717,46.252L245,214.653L91.219,370.393L43.502,324.14z"/>
    </svg>
)