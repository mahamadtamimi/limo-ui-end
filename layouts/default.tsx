import {Navbar} from "@/components/navbar";
import {Link} from "@nextui-org/link";
import {Head} from "./head";
import MainBar from "@/components/Global/MainMenu/MainMenu";
import localFont from "next/font/local";
import {NavbarTwo} from "@/components/navbarTwo";

const YekanBakh = localFont({
    src: './YekanBakh-VF.ttf',
    variable: '--yekan',
})
export default function DefaultLayout({
                                          children,
                                      }: {
    children: React.ReactNode;
}) {


    return (
        <div className={`${YekanBakh.variable}`}>
            <Head/>
            <Navbar/>
            <NavbarTwo/>
            <main>
                {children}
            </main>

        </div>
    );
}
