import postHeader from "@/api/define/postHead";

export async function loginSubmit(data: any) {

    const response: any = await fetch(`${process.env.API_PATH}/api/v1/auth/login`,{
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
    })
    return response;
}